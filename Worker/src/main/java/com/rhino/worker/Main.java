package main.java.com.rhino.worker;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import main.java.com.rhino.common.controller.CaptureController;
import main.java.com.rhino.common.model.Block;
import main.java.com.rhino.common.model.Campaign;
import main.java.com.rhino.common.model.Site;
import main.java.com.rhino.common.utils.Log;
import main.java.com.rhino.common.utils.StringUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by Vasiliy on 27.06.2016
 */
public class Main extends Application {

    private final static String TAG = Main.class.getSimpleName();
    private static final boolean DEBUG = false;

    private static String[] arguments = null;
    int TIMEOUT_DELAY = 4 * 60 * 1000 /* 5 minutes */;

    @Override public void start(final Stage primaryStage) throws Exception{
        Site site;
        try {
            Campaign camp = new Campaign(Integer.parseInt(arguments[0]));
            camp.setPath(arguments[1]);
            site = new Site(Integer.parseInt(arguments[2]), camp);
            site.setName(arguments[3]);
            site.setUrl(arguments[4]);
            site.setSelector(arguments[5]);
            site.setHeightPicture(Integer.parseInt(arguments[6]));
            site.setWidthPicture(Integer.parseInt(arguments[7]));
            site.setSize(Site.Size.values()[Integer.parseInt(arguments[8])]);
            site.setScript(StringUtils.Base64ToString(arguments[9]));
            Block block = new Block(Integer.parseInt(arguments[10]));
            site.setBlockId( block.getId());
            site.setBlock(block);
            block.setHeight(Integer.parseInt(arguments[11]));
            block.setWidth(Integer.parseInt(arguments[12]));
            block.setName(arguments[13]);
            block.setHtml(arguments[14]);
            block.setUrl(arguments[15]);
        } catch (Exception e){
            Log.e("worker", TAG, "Params get failed: " + e.getLocalizedMessage());
            System.exit(1);
            return;
        }
        final Scene scene = new Scene(new Pane());
        primaryStage.setTitle("Worker");
        primaryStage.setScene(scene);
        //primaryStage.show();
        primaryStage.setOpacity(0);
        Log.d(site.getName(), TAG, "Scene created");

        Timer timer = new Timer(TIMEOUT_DELAY, new AbstractAction() {
            @Override public void actionPerformed(ActionEvent e) {
                Log.e(site.getName(), TAG, "Task executing timeout (5 minutes)");
                System.exit(1);
            }
        });
        timer.start();

        Log.d(site.getName(), TAG, "Start capturing...");
        new CaptureController().Capture(site, result -> {
            Log.d(site.getName(), TAG, "Worker finished! result: " + result);
            System.exit(result ? 0 : 1);
        });
    }

    public static void main(String[] args) {
        arguments = args;
        if(DEBUG) {
            Log.d("worker", TAG, "Worker started! Arguments count: " + args.length);
            for (String arg : arguments)
                Log.d("worker", TAG, "\t " + arg);
        }
        try {
            launch(args);
        } catch (Throwable e) {
            Log.e("worker", TAG, e);
        }
    }
}
