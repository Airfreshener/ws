package main.java.com.rhino.common.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.com.rhino.common.model.Block;
import main.java.com.rhino.common.model.Campaign;
import main.java.com.rhino.common.model.Cookie;
import main.java.com.rhino.common.model.Site;
import main.java.com.rhino.common.utils.Log;
import main.java.com.rhino.common.utils.StringUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Vasiliy on 10.02.2016
 */
public class SqlController {

    //region == fields ==
    private final static String TAG = SqlController.class.getSimpleName();
    private final static String LOG_FILENAME = "sql_database";
    private static final int QUERY_TIMEOUT = 60; // in sec.
    private static SqlController sc = null;
    private static final int DB_VERSION = 6;
    //endregion == fields ==

    //region == controller ==

    private SqlController(){
        try {
            Class.forName("org.sqlite.JDBC");
            String versionText = getSetting("db_version", String.valueOf(DB_VERSION));
            Log.d(LOG_FILENAME, TAG, "DB version: " + versionText);
            Connection connection = createConnection();
            final Statement statement = connection.createStatement();
            statement.setQueryTimeout(QUERY_TIMEOUT);
            
            // Create tables
            createTableBlocks(statement);
            createTableSites(statement);
            createTableSettings(statement);
            createTableCampaign(statement);
            createTableCampSiteLinks(statement);
            createTableCookies(statement);
    
            // Update DB to actual version
            int current_version = Integer.parseInt(versionText.isEmpty() ? String.valueOf(DB_VERSION) : versionText);
            if (current_version < DB_VERSION) {
                updateDatabase(current_version, statement);
            }
            statement.close();
            connection.close();
            
            if(!versionText.equals(String.valueOf(DB_VERSION)))
                setSetting("db_version", String.valueOf(DB_VERSION));
            
        } catch (/*ClassNotFoundException |*/ SQLException e) {
            Log.e(LOG_FILENAME, TAG, e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    private Connection createConnection() throws SQLException {
        //Log.d(TAG, "Connect to DB");
        return DriverManager.getConnection("jdbc:sqlite:data.db");
    }

    private void updateDatabase(int current, Statement statement){
        int v = current + 1;
        Log.i(LOG_FILENAME, TAG, "update database from " + current + " to " + v + " version");
        try {
            switch (v) {

                case 6:
                    createTableCookies(statement);
                    break;
                case 5:
                    statement.executeUpdate("alter table `sites` add column `script` text default ''");
                    break;
                case 4:
                    createTableCampSiteLinks(statement);
                    statement.executeUpdate("alter table campaigns add column block_id integer default -1");
                    break;

                case 3:
                    createTableCampaign(statement);
                    break;

                case 2:
                    //added fields into sites table: dateStart, dateEnd, countInDay
                    try {
                        statement.executeUpdate("alter table sites add column date_end text default ''");
                        statement.executeUpdate("alter table sites add column date_start text default ''");
                        statement.executeUpdate("alter table sites add column count_in_day integer default 0");
                    } catch (SQLException e){
                        Log.e(LOG_FILENAME, TAG, e);
                    }
                    break;
            }
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        if(v < DB_VERSION)
            updateDatabase(v, statement);
    }

    private void createTableBlocks(Statement statement) throws SQLException{
        statement.executeUpdate("create table IF NOT EXISTS blocks (" +
                "id integer, " +
                "name text, " +
                "html text, " +
                "url text, " +
                "width integer, " +
                "height integer)");
    }

    private void createTableSites(Statement statement) throws SQLException{
        statement.executeUpdate("create table IF NOT EXISTS sites (" +
                "id integer, " +
                "name text, " +
                "url text, " +
                "block_id integer, " +
                "selector text, " +
                "picture_path text," +
                "width integer," +
                "height integer," +
                "date_start text," +
                "date_end text," +
                "count_in_day integer," +
                "`script` text)");
    }

    private void createTableCampaign(Statement statement) throws SQLException{
        statement.executeUpdate("create table IF NOT EXISTS campaigns (" +
                "id integer, " +
                "name text, " +
                "sites text, " +
                "path text, " +
                "date_start text," +
                "date_end text," +
                "block_id integer)");
    }

    private void createTableCookies(Statement statement) throws SQLException{
        statement.executeUpdate("create table IF NOT EXISTS cookies (" +
                "id integer, " +
                "site_id integer, " +
                "name text, " +
                "value text )");
    }

    private void createTableCampSiteLinks(Statement statement) throws SQLException{
        statement.executeUpdate("create table IF NOT EXISTS campsite_links (" +
                "camp_id integer, " +
                "site_id integer, " +
                "block_id integer)");
    }

    private void createTableSettings(Statement statement) throws SQLException{
        statement.executeUpdate("create table IF NOT EXISTS settings (k text, v text)");
    }

    public static SqlController getInstance(){
        if(sc == null)
            sc = new SqlController();
        return sc;
    }

    //endregion == controller ==

    //region == settings ==
    public Boolean getSetting(String key, boolean def) {
        return Boolean.parseBoolean(getSetting(key, def ? "true" : "false"));
    }
    public String getSetting(String key, String def){
        String value = null;
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("select * from settings where k='%s'", key));
            if(rs.next()) {
                value = rs.getString("v");
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return value  == null ? def : value;
    }
    public void setSetting(String key, String value){
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format("delete from settings where k='%s'", key));
            statement.executeUpdate(String.format("insert into settings values ('%s', '%s')", key, value));
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }
    //endregion == settings ==

    //region == blocks ==
    public int getNextBlockId(){
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select id from blocks order by id desc limit 1");
            int max = rs.next() ? rs.getInt("id") : -1;
            statement.close();
            connection.close();
            return max+1;
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return 0;
    }

    public ObservableList<Block> getBlocks(){
        Collection<Block> list = new ArrayList<>();
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from blocks");
            while(rs.next()) {
                Block block = new Block(rs.getInt("id"));
                block.setHtml(rs.getString("html"));
                block.setUrl(rs.getString("url"));
                block.setName(rs.getString("name"));
                block.setHeight(rs.getInt("height"));
                block.setWidth(rs.getInt("width"));
                list.add(block);
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return FXCollections.observableArrayList(list);
    }

    private Block getBlock(int id){
        Block block = null;
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("select * from blocks where id=%d", id));
            if(rs.next()) {
                block = new Block(rs.getInt("id"));
                block.setHtml(rs.getString("html"));
                block.setUrl(rs.getString("url"));
                block.setName(rs.getString("name"));
                block.setHeight(rs.getInt("height"));
                block.setWidth(rs.getInt("width"));
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return block;
    }

    public void addBlock(Block block){
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format("insert into blocks values (%d, '%s', '%s', '%s', %d, %d)",
                    block.getId(), block.getName(), block.getHtml(), block.getUrl(), block.getWidth(), block.getHeight()));
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

    public void updateBlock(Block block){
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format(
                    "update blocks set html='%s', name='%s', url='%s', width='%d', height='%d'  where id=%d",
                    block.getHtml(), block.getName(), block.getUrl(),
                    block.getWidth(), block.getHeight(), block.getId() ));
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

    public void deleteBlock(int id) {
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format("delete from blocks where id=%d", id));
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

    //endregion == blocks ==

    //region == sites ==
    public int getNextSiteId(){
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select id from sites order by id desc limit 1");
            int max = rs.next() ? rs.getInt("id") : -1;
            statement.close();
            connection.close();
            return max+1;
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return 0;
    }

    public ObservableList<Site> getSites(){
        Collection<Site> list = new ArrayList<>();
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from sites");
            while(rs.next()) {
                Site site = new Site(rs.getInt("id"), null);
                site.setBlockId(rs.getInt("block_id"));
                //site.setBlock(getBlock(rs.getInt("block_id")));
                site.setUrl(rs.getString("url"));
                site.setName(rs.getString("name"));
                site.setSelector(rs.getString("selector"));
                site.setHeightPicture(rs.getInt("height"));
                site.setWidthPicture(rs.getInt("width"));
                site.setCountInDay(rs.getInt("count_in_day"));
                site.setScript(StringUtils.Base64ToString(rs.getString("script")));
                list.add(site);
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return FXCollections.observableArrayList(list);
    }

    private ObservableList<Site> getSites(String ids, Campaign camp){
        Collection<Site> list = new ArrayList<>();
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("select * from sites where id in (%s)", ids));
            while(rs.next()) {
                Site site = new Site(rs.getInt("id"), camp);
                site.setBlockId(rs.getInt("block_id"));
                //site.setBlock(getBlock(rs.getInt("block_id")));
                site.setUrl(rs.getString("url"));
                site.setName(rs.getString("name"));
                site.setSelector(rs.getString("selector"));
                site.setHeightPicture(rs.getInt("height"));
                site.setWidthPicture(rs.getInt("width"));
                site.setCountInDay(rs.getInt("count_in_day"));
                site.setScript(StringUtils.Base64ToString(rs.getString("script")));
                list.add(site);
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return FXCollections.observableArrayList(list);
    }

    public Site getSite(int id, Campaign camp){
        Site site = null;
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("select * from sites where id=%d", id));
            while(rs.next()) {
                site = new Site(rs.getInt("id"), camp);
                site.setBlockId(rs.getInt("block_id"));
                //site.setBlock(getBlock(rs.getInt("block_id")));
                site.setUrl(rs.getString("url"));
                site.setName(rs.getString("name"));
                site.setSelector(rs.getString("selector"));
                site.setHeightPicture(rs.getInt("height"));
                site.setWidthPicture(rs.getInt("width"));
                site.setCountInDay(rs.getInt("count_in_day"));
                site.setScript(StringUtils.Base64ToString(rs.getString("script")));
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return site;
    }

    public void addSite(Site site){
        try {
            String q = String.format("insert into sites values (%d, '%s', '%s', %d, '%s', '%s', %d, %d, '%s', '%s', %d, '%s')",
                    site.getId(),
                    site.getName(),
                    site.getUrl(),
                    site.getBlockId(),
                    site.getSelector(),
                    "",
                    site.getWidthPicture(),
                    site.getHeightPicture(),
                    "",
                    "",
                    site.getCountInDay(),
                    StringUtils.StringToBase64(site.getScript()));
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(q);
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

    public void updateSite(Site site){
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format(
                    "update sites set " +
                            "name='%s', " +
                            "url='%s', " +
                            "block_id='%d', " +
                            "selector='%s', " +
                            "picture_path='%s', " +
                            "width='%d', " +
                            "height='%d', " +
                            "date_start='%s', " +
                            "date_end='%s', " +
                            "count_in_day=%d, " +
                            "`script`='%s' " +
                            " where id=%d",
                    site.getName(),
                    site.getUrl(),
                    site.getBlockId(),
                    site.getSelector(),
                    "",
                    site.getWidthPicture(),
                    site.getHeightPicture(),
                    "",
                    "",
                    site.getCountInDay(),
                    StringUtils.StringToBase64(site.getScript()),
                    site.getId())
            );
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

    public void deleteSite(int id) {
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format("delete from sites where id=%d", id));
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }
    //endregion == sites ==

    //region == camps ==
    public int getNextCampId(){
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select id from campaigns order by id desc limit 1");
            int max = rs.next() ? rs.getInt("id") : -1;
            rs.close();
            statement.close();
            connection.close();
            return max+1;
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return 0;
    }

    public ObservableList<Campaign> getCamps(){
        Collection<Campaign> list = new ArrayList<>();
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from campaigns");
            while(rs.next()) {
                Campaign camp = new Campaign(rs.getInt("id"));
                camp.setName(rs.getString("name"));
                camp.setSitesId(rs.getString("sites"));
                //camp.setSites(getSites(rs.getString("sites"), camp));
                camp.setPath(rs.getString("path"));
                camp.setDateEnd(rs.getString("date_end"));
                camp.setDateStart(rs.getString("date_start"));
                camp.setBlockId(rs.getInt("block_id"));
                //camp.setBlock(getBlock(rs.getInt("block_id")));
                list.add(camp);
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return FXCollections.observableArrayList(list);
    }

/*
    public Campaign getCamp(int id, boolean fillRefs){
        Campaign camp = null;
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("select * from campaigns where id=%d",id));
            if(rs.next()) {
                camp = new Campaign(rs.getInt("id"));
                camp.setName(rs.getString("name"));
                if(fillRefs)
                    camp.setSites(getSites(rs.getString("sites"), camp));
                camp.setPath(rs.getString("path"));
                camp.setDateEnd(rs.getString("date_end"));
                camp.setDateStart(rs.getString("date_start"));
                if(rs.getInt("block_id") >= 0)
                    camp.setBlock(getBlock(rs.getInt("block_id")));
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(TAG, e);
        }
        return camp;
    }
*/

    public void addCamp(Campaign camp){
        try {
            String[] sitesId = new String[camp.getSites().size()];
            for(int i = 0 ; i < camp.getSites().size() ; i++){
                sitesId[i] = String.valueOf(camp.getSites().get(i).getId());
            }
            String q = String.format("insert into campaigns (id, name, sites, path, date_start, date_end, block_id ) values('%d', '%s', '%s', '%s', '%s', '%s', %d)",
                    camp.getId(),
                    camp.getName(),
                    StringUtils.Join(sitesId, ",", false),
                    camp.getPath(),
                    camp.getDateStart(),
                    camp.getDateEnd(),
                    camp.getBlockId());
    
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(q);
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

    public void updateCamp(Campaign camp){
        try {
            String[] sitesId = new String[camp.getSites().size()];
            for(int i = 0 ; i < camp.getSites().size() ; i++){
                sitesId[i] = String.valueOf(camp.getSites().get(i).getId());
            }
            String q = String.format(
                    "update campaigns set name='%s', sites='%s', path='%s', date_start='%s', date_end='%s', block_id='%d' where id=%d",
                    camp.getName(),
                    StringUtils.Join(sitesId, ",", false),
                    camp.getPath(),
                    camp.getDateStart(),
                    camp.getDateEnd(),
                    camp.getBlockId(),
                    camp.getId());
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(q);
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

    public void deleteCamp(int id) {
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format("delete from campaigns where id=%d", id));
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }
    //endregion == camps ==
    
    //region == cookies ==
    public List<String> getCookies(int site_id){
        List<String> list = new ArrayList<>();
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("select * from cookies where site_id=%d", site_id));
            while(rs.next()) {
                //Cookie c = new Cookie();
                //c.id = rs.getInt("id");
                //c.site_id = rs.getInt("site_id");
                //Log.d(LOG_FILENAME, TAG, "cookies row # " + rs.getRow());
                String name = StringUtils.Base64ToString(rs.getString("name"));
                String value = StringUtils.Base64ToString(rs.getString("value"));
                list.add(name + "=" + value);
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            Log.e(LOG_FILENAME, TAG, e);
        }
        return list;
    }
    
    public void setCookies(Iterable<Cookie> cookies, int site_id){
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format("delete from cookies where site_id=%d", site_id));
            for(Cookie c : cookies) {
                statement.executeUpdate(String.format("insert into cookies (id, site_id, name, value) values('%d', '%d', '%s', '%s')",
                        c.id, c.site_id, StringUtils.StringToBase64(c.name), StringUtils.StringToBase64(c.value)));
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }
    
    //endregion == cookies ==

    public void setCampSiteLink(int camp_id, int site_id, int block_id){
        try {
            String q = String.format("insert into campsite_links (camp_id, site_id, block_id) values('%d', '%d', '%d')",
                    camp_id, site_id, block_id);
    
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(q);
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

    public Block getCampSiteLinkBlock(int camp_id, int site_id){
        int block_id = -1;
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("select * from campsite_links where camp_id=%d and site_id=%d", camp_id, site_id));
            if(rs.next()) {
                block_id = rs.getInt("block_id");
            }
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
        return block_id == -1 ? null : getBlock(block_id);
    }

    public void deleteCampSiteLink(int camp_id, int site_id) {
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format("delete from campsite_links where camp_id=%d and site_id=%d", camp_id, site_id));
            statement.close();
            connection.close();
        } catch (SQLException e){
            Log.e(LOG_FILENAME, TAG, e);
        }
    }

}
