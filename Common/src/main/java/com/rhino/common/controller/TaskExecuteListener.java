package main.java.com.rhino.common.controller;

/**
 * Created by Vasiliy on 27.06.2016
 */
public interface TaskExecuteListener {
    void executeFinished(boolean result);
}
