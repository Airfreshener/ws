package main.java.com.rhino.common.controller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.embed.swing.JFXPanel;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import main.java.com.rhino.common.model.Block;
import main.java.com.rhino.common.model.Site;
import main.java.com.rhino.common.utils.Const;
import main.java.com.rhino.common.utils.Log;
import main.java.com.rhino.common.utils.WebUtils;
import netscape.javascript.JSException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by Vasiliy on 29.02.2016
 */
public class CaptureController {

    private final static String TAG = CaptureController.class.getSimpleName();
    private final static boolean DEBUG = true;

    private final static int IMAGE_WIDTH = 1500, IMAGE_HEIGHT = 2000;
    private final static int LOADING_LIMIT_TIME = 10000;
    private TaskExecuteListener executeListener;
    private CookieManager cookieManager;


    public void Capture(Site site, TaskExecuteListener executeListener){
        this.executeListener = executeListener;
        if(site == null){
            if(executeListener != null)
                executeListener.executeFinished(false);
            return;
        }
        int sizeW = (site.getWidthPicture() == 0 ? IMAGE_WIDTH : site.getWidthPicture()) - 2;
        int sizeH = site.getHeightPicture() == 0 ? IMAGE_HEIGHT : site.getHeightPicture();
        WebView wvRenderer = new WebView();
        JFXPanel p = new JFXPanel();
        wvRenderer.setPrefSize(sizeW, sizeH);
        p.setScene(new Scene(wvRenderer));
        p.setSize(sizeW, sizeH);
        p.setVisible(false);
        ChangeListener<Worker.State> listener = new CaptureListener(wvRenderer, site, p);
        URL res = getClass().getClassLoader().getResource("css/style.css");
        if(res != null)
            wvRenderer.getEngine().setUserStyleSheetLocation(res.toExternalForm());
        wvRenderer.getEngine().getLoadWorker().stateProperty().addListener(listener);
        wvRenderer.getEngine().setUserAgent(Const.USER_AGENT); // fake it like google chrome
    
        if(DEBUG) Log.d(site.getName(), TAG, "Set cookies...");
        try {
            cookieManager = new java.net.CookieManager();
            CookieHandler.setDefault(cookieManager);
            //SqlController sql = SqlController.getInstance();
            //List<String> cookies = sql.getCookies(site.getId());
            //Map<String, List<String>> headers = new LinkedHashMap<>();
            //headers.put("Set-Cookie", cookies);
            //cookieManager.put(URI.create(site.getUrl()), headers);
        } catch (Exception e){
            e.printStackTrace();
        }
    
        if(DEBUG) Log.d(site.getName(), TAG, "Page load url: " + site.getUrl());
        wvRenderer.getEngine().load(site.getUrl());
        if(DEBUG) Log.d(site.getName(), TAG, "Page load started.");
    }

    private class CaptureListener implements ChangeListener<Worker.State> {

        private WebEngine engine;
        private JFXPanel panel;
        private Site site;
        private WebView webView;
        
        private Timer timer = new Timer(LOADING_LIMIT_TIME, new AbstractAction() {
            @Override public void actionPerformed(ActionEvent e) {
                Platform.runLater(() -> {
                    timer.stop();
                    try {
                        engine.getLoadWorker().stateProperty().removeListener(CaptureListener.this);
                    } catch(Exception ignored) {}
                    do_work();

                });
            }
        });

        CaptureListener(WebView webView, Site site, JFXPanel panel){
            this.webView = webView;
            this.engine = webView.getEngine();
            this.panel = panel;
            this.site = site;
            timer.start();
        }

        @Override public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
            if(DEBUG) Log.d(site.getName(), TAG, "Page status: " + newValue);
            if (newValue == Worker.State.SUCCEEDED) {
                Platform.runLater(() -> {
                    try {
                        engine.getLoadWorker().stateProperty().removeListener(CaptureListener.this);
                    } catch (Exception ignored){}
                    timer.stop();
                    do_work();
                });
            } else if(newValue == Worker.State.FAILED){
                Log.e(site.getName(), TAG, "Loading page (" + site.getName() + ") failed!");
                close(false);
            }
        }

        private void do_work(){
            try {
                engine.getLoadWorker().stateProperty().removeListener(this);
            } catch (Exception ignored){}
            Log.d(site.getName(), TAG, "Page for capture loaded: " + site.getUrl());
            Block b = site.getBlock();

            final String title = (String) engine.executeScript("document.title");
            boolean replace = false;
            if(b != null){
                replace = WebUtils.ReplaceBlock(b.getUrl(), b.getHtml(), site.getSelector(), site.getScript(), b.getWidth(), b.getHeight(),engine, site.getName());
            }
            if(DEBUG) Log.d(site.getName(), TAG, "Replace done: " + replace);
            
            if(site.getScript() != null && !site.getScript().isEmpty())
                Log.d(site.getName(), TAG, "Run script: " + WebUtils.RunScript(site.getScript(), engine, site.getName()));
    
            background(site, () ->{
                final int AFTER_REPLACE_FIX = 20000, AFTER_REPLACE_RANDOM = 10000;
                Thread.sleep(AFTER_REPLACE_FIX + rand(AFTER_REPLACE_RANDOM));
    
                ui(site, () -> {
        
                    //fix size capture
                    if(site.getSize() == Site.Size.BOTH || site.getSize() == Site.Size.FIXSIZE)
                        captureView(site.getUrl(), title, false);
        
                    //full size capture
                    if(site.getSize() == Site.Size.BOTH || site.getSize() == Site.Size.FULLSIZE) {
                        try {
                            float height = (Integer)engine.executeScript("var body = document.body, html = document.documentElement; var height = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); height;");
                            //height = Math.min(height, 10000);
                            engine.executeScript("window.scrollTo(0, 0);");
                
                            panel.setSize(panel.getWidth(), (int) height);
                            webView.resize(panel.getWidth(), (int) height);
                            webView.setPrefSize(panel.getWidth(), (int) height);
                            webView.setMinHeight(height);
                            Log.d(site.getName(), TAG, "changed height: " + height);
                        } catch (JSException e) {
                            Log.e(site.getName(), TAG, e);
                        }
                        background(site, () -> {
                            final int AFTER_RESIZE_FIX = 10000, AFTER_RESIZE_RANDOM = 10000;
                            Thread.sleep(AFTER_RESIZE_FIX + rand(AFTER_RESIZE_RANDOM));
                            ui(site, () -> {
                                try {
                                    if (site.getScript() != null && !site.getScript().isEmpty())
                                        Log.d(site.getName(), TAG, "Run script: " + WebUtils.RunScript(site.getScript(), engine, site.getName()));
                                } catch (JSException e) {
                                    Log.e(site.getName(), TAG, e);
                                }
                                background(site, () -> {
                                    final int AFTER_SCRIPT_FIX = 10000, AFTER_SCRIPT_RANDOM = 10000;
                                    Thread.sleep(AFTER_SCRIPT_FIX + rand(AFTER_SCRIPT_RANDOM));
                                    ui(site, () -> close(captureView(site.getUrl(), title, true)));
    
                                });
                            });
                        });
                    } else {
                        close(false);
                    }
                });
            });
        }

        private void close(final boolean result){
            
/*
            SqlController sql = SqlController.getInstance();
            Collection<Cookie> cookies = new ArrayList<>();
            List<HttpCookie> list = cookieManager.getCookieStore().getCookies();
            for (HttpCookie c : list){
                Cookie cookie = new Cookie();
                cookie.name = c.getName();
                cookie.value = c.getValue();
                cookie.site_id = site.getId();
                cookies.add(cookie);
            }
            sql.setCookies(cookies, site.getId());
*/
            
            
            engine.load("about:blank");
            cookieManager.getCookieStore().removeAll();
            panel.getScene().getWindow().hide();
            panel = null;
            webView = null;
            engine = null;
            site = null;
            if(executeListener != null)
                Platform.runLater(() -> executeListener.executeFinished(result));
        }

        private BufferedImage loadImage(String strUrl){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            InputStream is = null;
            BufferedImage bi = null;
            try {
                URL url = new URL(strUrl);
                is = url.openStream ();
                final int BUFFER_SIZE = 4096;
                byte[] byteChunk = new byte[BUFFER_SIZE]; // Or whatever size you want to read in at a time.
                int n;

                while ( (n = is.read(byteChunk)) > 0 ) {
                    stream.write(byteChunk, 0, n);
                }

                InputStream in = new ByteArrayInputStream(stream.toByteArray());
                bi = ImageIO.read(in);

            }
            catch (Exception e) {
                Log.e(site.getName(), TAG, String.format("Failed while reading bytes from %s: %s", strUrl, e.getMessage()));
                // Perform any other exception handling that's appropriate.
            }
            finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        Log.e(site.getName(), TAG, e);
                    }
                }
            }
            return bi;
        }

        @SuppressWarnings("MagicNumber")
        private boolean captureView(String url, String title, boolean fullSize) {
            //final int w = 1280, h = Math.min(5000, panel.getHeight());
            try {
                final int w = 1280, h = panel.getHeight();
    
                ClassLoader cl = getClass().getClassLoader();
                URL header = cl.getResource("images/header2.png");
                URL footer = cl.getResource("images/footer2.png");
                URL bar = cl.getResource("images/bar_2.png");
                URL arrow_up = cl.getResource("images/arrow_up_2.png");
                URL arrow_down = cl.getResource("images/arrow_down_2.png");
                URL empty_area = cl.getResource("images/empty_area_2.png");
    
                if (footer == null || header == null || bar == null ||
                        arrow_down == null || arrow_up == null || empty_area == null) {
                    Log.e(site.getName(), TAG, "Capture failed! Can not load image from resources.");
                    return false;
                }
    
                Image imageFooter = new ImageIcon(footer).getImage();
                Image imageHeader = new ImageIcon(header).getImage();
                Image imageBar = new ImageIcon(bar).getImage();
                Image imageArrowUp = new ImageIcon(arrow_up).getImage();
                Image imageArrowDown = new ImageIcon(arrow_down).getImage();
                Image imageEmptyArea = new ImageIcon(empty_area).getImage();
                int heightHeader = imageHeader.getHeight(null);
                int heightFooter = imageFooter.getHeight(null);
    
                if (DEBUG) Log.d(site.getName(), TAG, "Capture page image. Height: " + h);
    
                //BufferedImage biPanel = new BufferedImage(frame.getWidth(), h, BufferedImage.TYPE_INT_ARGB);
                //java.awt.Graphics graphicsPanel = biPanel.createGraphics();
                //panel.paint(graphicsPanel);
                if (DEBUG) Log.d(site.getName(), TAG, "Create snapshot...");
                WritableImage snapshot = webView.snapshot(null, null);
                if (DEBUG) Log.d(site.getName(), TAG, "Snapshot created. Create buffered image...");
                BufferedImage biContent = SwingFXUtils.fromFXImage(snapshot, null);
                if (DEBUG) Log.d(site.getName(), TAG, "Buffered image creates.");
    
                BufferedImage favIcon = loadImage("http://www.google.com/s2/favicons?domain=" + url);
                if (DEBUG) Log.d(site.getName(), TAG, String.format("Loaded favicon: %b", favIcon != null));
    
                if (DEBUG) Log.d(site.getName(), TAG, "Create bitmap for drawing...");
                BufferedImage biUi = new BufferedImage(w, h + heightFooter + heightHeader, BufferedImage.TYPE_INT_ARGB);
                Graphics graphicsUi = biUi.createGraphics();
    
                int lineFooter = h + heightHeader;
    
    
                if (DEBUG) Log.d(site.getName(), TAG, "draw header...");
                //draw footer and header
                graphicsUi.drawImage(imageHeader, 0, 0, null);
                if (DEBUG) Log.d(site.getName(), TAG, "draw footer...");
                graphicsUi.drawImage(imageFooter, 0, lineFooter, null);
    
                //draw favicon
                if (DEBUG) Log.d(site.getName(), TAG, "draw favicon...");
                if (favIcon != null) {
                    graphicsUi.drawImage(favIcon, 445, 7, null);
                    graphicsUi.drawImage(favIcon, 81, 6, null);
                }
    
                //draw url
                if (DEBUG) Log.d(site.getName(), TAG, "draw url...");
                final int maxLengthUrl = 50;
                Font font = new Font("Tahoma", Font.PLAIN, 11);
                //Map<TextAttribute, Object> attributes = new HashMap<TextAttribute, Object>();
                //attributes.put(TextAttribute.TRACKING, -0.05);
                //font = font.deriveFont(attributes);
                graphicsUi.setFont(font);
                graphicsUi.setColor(Color.BLACK);
                graphicsUi.drawString(url.length() > maxLengthUrl ? url.substring(0, maxLengthUrl - 3) + "..." : url, 102, 19);
    
                //draw title
                if (DEBUG) Log.d(site.getName(), TAG, "draw title...");
                final int maxLengthTitle = 27;
//            font = new Font("Verdana", Font.PLAIN, 11);
                graphicsUi.setFont(font);
                graphicsUi.drawString(title.length() > maxLengthTitle ? title.substring(0, maxLengthTitle - 3) + "..." : title, 466, 19);
    
                //draw time
                if (DEBUG) Log.d(site.getName(), TAG, "draw current time...");
                DateFormat timeDrawFormat = new SimpleDateFormat("H:mm");
                DateFormat dateDrawFormat = new SimpleDateFormat("dd.MM.yyyy");
                Date date = new Date();
                String strDateDraw = dateDrawFormat.format(date);
                String strTimeDraw = timeDrawFormat.format(date);
                font = new Font("Tahoma", Font.PLAIN, 11);
                graphicsUi.setFont(font);
                graphicsUi.drawString(strTimeDraw, 1209, lineFooter + 19);
                graphicsUi.drawString(strDateDraw, 1194, lineFooter + 34);
    
                //draw content
                //graphicsUi.drawImage(biPanel, 0, heightHeader, null);
                if (DEBUG) Log.d(site.getName(), TAG, "draw web page content...");
                graphicsUi.drawImage(biContent, 0, heightHeader, null);
    
                //draw scrollbar (override content from right)
                final float wBar = 16;
                if (DEBUG) Log.d(site.getName(), TAG, "get page height...");
                float height = (Integer) engine.executeScript("var body = document.body, html = document.documentElement; var height = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); height;");
                //float length = Math.max((float)h * ((float)h/height), 20);
                float hEmpty = imageEmptyArea.getHeight(null);
                if (DEBUG) Log.d(site.getName(), TAG, "draw scrollbar...");
                for (int i = 0; i < (float) h / hEmpty; i++)
                    graphicsUi.drawImage(imageEmptyArea, (int) (w - wBar), (int) (heightHeader + i * hEmpty), null);
                graphicsUi.drawImage(imageArrowUp, (int) (w - wBar), heightHeader, null);
                if (height > h && !fullSize) {
                    float hArrowUp = imageArrowUp.getHeight(null);
                    float area = h - imageArrowDown.getHeight(null) - hArrowUp;
                    float scroll = (Integer) engine.executeScript("Math.floor( document.body.scrollTop);");
                    float barEnd = area * (scroll + h) / height;
                    float barStart = area * scroll / (height - h);
                    for (int i = (int) barStart; i < barEnd; i++) {
                        graphicsUi.drawImage(imageBar, (int) (w - wBar), (int) (heightHeader + hArrowUp + i), null);
                    }
                }
                graphicsUi.drawImage(imageArrowDown, (int) (w - wBar), lineFooter - imageArrowDown.getHeight(null), null);
                if (DEBUG) Log.d(site.getName(), TAG, "draw done!");
    
    
                //save to file
    
                if (DEBUG) Log.d(site.getName(), TAG, "save to file...");
                String path = site.getCamp().getPath();
                try {
        
                    Log.d(site.getName(), TAG, "Save captured image to path: " + path);
        
                    DateFormat dateFormat = new SimpleDateFormat("dd_MM_yy_(HH_mm)");
                    String time = dateFormat.format(date);
                    File outFile = new File(String.format("%s/%s/%s_%s.png", path, site.getName(), fullSize ? "full" : "fix", time));
                    File directory = new File(outFile.getParentFile().getAbsolutePath());
                    if (!directory.exists()) Log.d(site.getName(), TAG, "Create output dir: " + directory.mkdirs());
                    Log.d(site.getName(), TAG, "final path of file: " + outFile.getAbsolutePath());
                    boolean result = ImageIO.write(biUi, "PNG", outFile);
                    Log.d(site.getName(), TAG, "save captured image to file: " + result);
                } catch (IOException e) {
                    Log.e(site.getName(), TAG, e);
                }
                //graphicsPanel.dispose();
                //biPanel.flush();
                biContent.flush();
                graphicsUi.dispose();
                biUi.flush();
                return true;
            } catch (Exception e){
                Log.e(site.getName(), TAG, e);
                return false;
            }
        }
    }

    interface SafeRunnable {
        void run() throws Exception;
    }
    private static void background(Site site, SafeRunnable r){
        new Thread(() -> {
            try {
                r.run();
            } catch (Exception e) {
                Log.e(site.getName(), TAG, e);
            }
        }).start();
    }
    
    private static void ui(Site site, SafeRunnable r){
        Platform.runLater(() -> {
            try {
                r.run();
            } catch (Exception e) {
                Log.e(site.getName(), TAG, e);
            }
        });
    }
    
    private static int rand(int max){
        return new Random().nextInt(max);
    }
    
}
