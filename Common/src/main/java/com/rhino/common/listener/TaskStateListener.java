package main.java.com.rhino.common.listener;

import main.java.com.rhino.common.model.Site;

/**
 * Created by Vasiliy on 27.06.2016
 */
public interface TaskStateListener {
    void taskStopped(Site site);
}
