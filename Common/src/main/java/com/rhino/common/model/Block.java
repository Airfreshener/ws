package main.java.com.rhino.common.model;

/**
 * Created by Vasiliy on 09.02.2016
 */
public class Block {
    private int id;
    private String url = "";
    private String html = "";
    private String name = "";
    private int width = 0;
    private int height = 0;

    public Block(Block b){
        id = b.id;
        url = b.url;
        html = b.html;
        name = b.name;
        width = b.width;
        height = b.height;
    }

    public Block(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url != null ? url : "";
    }

    public Block setUrl(String url) {
        this.url = url; return this;
    }

    public String getHtml() {
        return html;
    }

    public Block setHtml(String html) {
        this.html = html; return this;
    }

    public String getName() {
        return name != null ? name : "";
    }

    public Block setName(String name) {
        this.name = name; return this;
    }

    @Override public String toString(){
        return name;
    }

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public Block setWidth(int width) {
        this.width = width; return this;
    }

    public int getHeight() {
        return height;
    }

    public Block setHeight(int height) {
        this.height = height; return this;
    }
}
