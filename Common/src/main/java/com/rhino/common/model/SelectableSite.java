package main.java.com.rhino.common.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * Created by Vasiliy on 06.11.2016
 */
public class SelectableSite extends Site {

    public BooleanProperty selected;

    public SelectableSite(Site site, Campaign camp) {
        super(site, camp);
        this.selected = new SimpleBooleanProperty(false);
    }

    public void setSelected(boolean selected) {
        this.selected.set(selected);
    }
    public BooleanProperty selectedProperty() {
        return selected;
    }
}
