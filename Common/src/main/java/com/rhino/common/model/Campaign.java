package main.java.com.rhino.common.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.com.rhino.common.utils.ArrayUtils;
import main.java.com.rhino.common.utils.StringUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasiliy on 30.10.2016
 */
public class Campaign {

    private int id;
    private List<Integer> sitesId = new ArrayList<>();
    private ObservableList<Site> sites = FXCollections.observableArrayList(new ArrayList<Site>());
    private String name = "";
    private String path = "";
    private int block_id = -1;
    private Block block = null;
    private LocalDate dateStart = null;
    private LocalDate dateEnd = null;

    private boolean running = false;


    public synchronized void setRunning(boolean running){
        this.running = running;
    }

    public synchronized  boolean isRunning (){
        return running;
    }

    public Campaign(int id) {
        this.id = id;
    }

    @Override public String toString(){
        return name;
    }

    public ObservableList<Site> getSites() {
        return sites;
    }
    public Campaign setSites(Iterable<Site> sites) {
        this.sites.clear();
        for(Site site : sites)
            if(ArrayUtils.IsInList(sitesId, site.getId())) {
                this.sites.add(new Site(site, this));
            }
        return this;
    }

    public List<Integer> getSitesId() {
        return sitesId;
    }
    public Campaign setSitesId(String strSitesId) {
        List<String> list = StringUtils.Split(strSitesId, ",");
        for(String id : list) {
            sitesId.add(Integer.parseInt(id));
        }
        return this;
    }
    
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public Campaign setName(String name) {
        this.name = name;
        return this;
    }

    public Block getBlock() { return block; }
    public Campaign setBlock(Iterable<Block> blocks) {
        if(block_id != -1)
            for (Block block : blocks)
                if(block.getId() == block_id)
                    this.block = block;
        return this;
    }
    public Campaign setBlock(Block block) {
        this.block = block;
        return this;
    }
    
    public int getBlockId() { return block_id; }
    public Campaign setBlockId(int blockId) { this.block_id = blockId; return this; }
    
    public String getPath() {
        return path;
    }
    public Campaign setPath(String path) {
        this.path = path;
        return this;
    }

    public LocalDate getDateStart() {
        return dateStart;
    }
    public String getDateStartStr() {
        return dateStart == null ? "" : dateStart.toString();
    }

    public void setDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
    }
    public void setDateStart(String dateStart) {
        LocalDate date = dateStart == null || dateStart.isEmpty() || dateStart.equals("null") ? null : LocalDate.parse(dateStart);
        setDateStart(date);
    }

    public LocalDate getDateEnd() {
        return dateEnd;
    }
    public String getDateEndStr() {
        return dateEnd == null ? "" : dateEnd.toString();
    }
    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd = dateEnd;
    }
    public void setDateEnd(String dateEnd) {
        LocalDate date = dateEnd == null || dateEnd.isEmpty() || dateEnd.equals("null") ? null : LocalDate.parse(dateEnd);
        setDateEnd(date);
    }

}
