package main.java.com.rhino.common.model;

/**
 * Created by Vasiliy on 09.02.2016
 */
public class Site {

    private final Campaign camp;
    private int id;
    private String url = "";
    private int block_id = -1;
    private Block block = null;
    private String name = "";
    private String selector = "";
    private String script = "";
    private int widthPicture = 0;
    private int heightPicture = 0;
    private int countInDay = 1;
    private Size size = Size.BOTH;

    public Site(int id, Campaign camp) {
        this.id = id;
        this.camp = camp;
    }

    public Site(Site site, Campaign camp){
        this.camp = camp;
        update(site);
    }

    public void update(Site site){
        id = site.id;
        url = site.url;
        block = site.block;
        name = site.name;
        selector = site.selector;
        widthPicture = site.widthPicture;
        heightPicture = site.heightPicture;
        countInDay = site.countInDay;
        size = site.size;
        script = site.script;
    }

    public Size getSize() { return size; }
    public void setSize(Size size) { this.size = size; }

    public enum Size { FULLSIZE, FIXSIZE, BOTH }

    @Override public String toString(){ return name; }

    public int getId() { return id; }

    public Campaign getCamp() { return camp; }


    public String getUrl() { return url != null ? url : ""; }
    public Site setUrl(String url) { this.url = url; return this; }

    public Block getBlock() { return block; }
    public Site setBlock(Iterable<Block> blocks) {
        if(block_id != -1)
            for(Block block : blocks)
                if(block.getId() == block_id)
                    this.block = block;
        return this;
    }
    public Site setBlock(Block block) {
        this.block = block;
        return this;
    }

    public String getSelector() { return selector; }
    public Site setSelector(String selector) { this.selector = selector; return this; }

    public int getWidthPicture() { return widthPicture; }
    public Site setWidthPicture(int widthPicture) { this.widthPicture = widthPicture; return this; }

    public int getBlockId() { return block_id; }
    public Site setBlockId(int blockId) { this.block_id = blockId; return this; }

    public int getHeightPicture() { return heightPicture; }
    public Site setHeightPicture(int heightPicture) { this.heightPicture = heightPicture; return this; }

    public String getName() { return name != null ? name : ""; }
    public Site setName(String name) { this.name = name; return this; }

    public int getCountInDay() { return countInDay; }
    public void setCountInDay(int countInDay) { this.countInDay = Math.max(countInDay, 1); }

    public String getScript() { return script; }
    public void setScript(String script) { this.script = script; }


}
