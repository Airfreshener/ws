package main.java.com.rhino.common.model;

/**
 * Created by Vasiliy on 13.04.2017
 */
public class Cookie {
    public String name;
    public String value;
    public int id = 0;
    public int site_id;
    
    @Override
    public String toString(){
        return name + "=" + value;
    }
}
