package main.java.com.rhino.common.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Created by Vasiliy on 07.11.2016
 */
public class CustomFormatter extends Formatter {

    private static final String format = "%1$td.%1$tm %1$tH:%1$tM:%1$tS %2$s %3$s%4$s%n";
    private final Date dat = new Date();

    @Override
    public synchronized String format(LogRecord record) {
        dat.setTime(record.getMillis());
        String message = formatMessage(record);
        String throwable = "";
        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            pw.println();
            record.getThrown().printStackTrace(pw);
            pw.close();
            throwable = sw.toString();
        }
        String level;
        if(record.getLevel().intValue() == Level.FINEST.intValue()){
            level = "DEBUG";
        } else if(record.getLevel().intValue() == Level.WARNING.intValue()){
            level = "WARNING";
        } else if(record.getLevel().intValue() == Level.SEVERE.intValue()){
            level = "ERROR";
        } else {
            level = "INFO";
        }
        return String.format(format,
                dat,
                level,
                message,
                throwable);
    }
}
