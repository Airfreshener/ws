package main.java.com.rhino.common.log;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

/**
 * Created by Vasiliy on 07.05.2017
 */
public class ConsoleHandler extends StreamHandler {
    
    private void configure() {
        setLevel(Level.ALL);
        setFilter(null);
        setFormatter(new CustomFormatter());
        try {
            setEncoding(null);
        } catch (Exception ignored) { }
    }
    
    public ConsoleHandler() {
        super(System.out, new CustomFormatter());
        configure();
    }
    
    @Override
    public void publish(LogRecord record) {
        super.publish(record);
        flush();
    }
    
    @Override
    public void close() {
        flush();
    }
}
