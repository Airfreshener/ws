package main.java.com.rhino.common.utils;

import java.util.List;

/**
 * Created by Vasiliy on 13.04.2017
 */
public class ArrayUtils {
    
    public static boolean IsInList(List<String> list, String target){
        if(target != null && list != null && list.size() > 0) {
            for (String s : list)
                if (s != null && target.equals(s))
                    return true;
        }
        return false;
    }

    public static boolean IsInList(List<Integer> list, Integer target){
        if(target != null && list != null && list.size() > 0) {
            for (Integer s : list)
                if (s != null && target.equals(s))
                    return true;
        }
        return false;
    }
}
