package main.java.com.rhino.common.utils;

/**
 * Created by Vasiliy on 11.10.2016
 */
public class Const {

    public final static String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36";
    public final static String SEARCH_SITE_IN_URL = "search_site_in_url";
    public final static String SEARCH_BLOCK_IN_PATH = "search_site_in_path";
}
