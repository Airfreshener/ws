package main.java.com.rhino.common.utils;

import com.sun.istack.internal.NotNull;
import main.java.com.rhino.common.log.ConsoleHandler;
import main.java.com.rhino.common.log.CustomFormatter;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Vasiliy on 28.02.2016
 */
public abstract class Log {
    
    private final static Map<String, Logger> loggers = new HashMap<>();
    private static Logger loggerCommon;
    private static final int KB = 1024;
    static {
        loggerCommon = initLogger("common");
    }
    
    private static String getLogFileName(){
        return "common";
    }

    @NotNull
    private synchronized static Logger initLogger(String filename){
        if(loggers.containsKey(filename))
            return loggers.get(filename);
        else {
            Logger logger = Logger.getLogger(filename);
            logger.setLevel(Level.ALL);
            try {
                File dirLog = new File("logs");
                if (!dirLog.exists()) dirLog.mkdirs();
                FileHandler fh = new FileHandler("logs/" + filename + "_%g.log", 5 * KB * KB, 10, true);
                fh.setFormatter(new CustomFormatter());
                logger.addHandler(fh);
                logger.addHandler(new ConsoleHandler());
                loggers.put(filename, logger);
                return logger;
            } catch (IOException e) {
                e.printStackTrace();
                if(filename.equals(getLogFileName()) && loggerCommon != null)
                    return loggerCommon;
                else
                    throw new RuntimeException("Can not init log file: " + e.getMessage(), e);
            }
        }
    }

    private static String cutTag(String tag){
        return tag.replace("main.java.com.rhino.", "");
    }
    private static String log(String tag, String msg){ return String.format("%s: %s", cutTag(tag), msg); }

    public synchronized static void d(String tag, String message){ d(getLogFileName(), tag, message); }
    //public synchronized static void w(String tag, String message){ w(getLogFileName(), tag, message); }
    public synchronized static void e(String tag, String message){ e(getLogFileName(), tag, message); }
    //public synchronized static void i(String tag, String message){ i(getLogFileName(), tag, message); }
    public synchronized static void e(String tag, Throwable e)   { e(getLogFileName(), tag, e); }

    public synchronized static void d(String filename, String tag, String message){ initLogger(filename).log(Level.FINEST, log(tag, message)); }
    public synchronized static void w(String filename, String tag, String message){ initLogger(filename).log(Level.WARNING, log(tag, message)); }
    public synchronized static void e(String filename, String tag, String message){ initLogger(filename).log(Level.SEVERE, log(tag, message)); }
    public synchronized static void i(String filename, String tag, String message){ initLogger(filename).log(Level.FINE, log(tag, message)); }
    public synchronized static void e(String filename, String tag, Throwable e   ){ initLogger(filename).log(Level.SEVERE, cutTag(tag) + ": Exception!", e); }

}
