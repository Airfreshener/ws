package main.java.com.rhino.common.utils;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Created by Vasiliy on 05.11.2016
 */
public class StringUtils {

    public static List<String> Split(String src, String separator){
        List<String> list = new ArrayList<>();
        if(src == null || src.equals("null")) return list;
        String[] arr = (src.split(separator, 0));
        for (String anArr : arr) {
            if (anArr != null && !anArr.isEmpty()) list.add(anArr);
        }
        return list;
    }

    public static String Join(List<?> list, String separator, boolean keepEmpty){
        StringBuilder sb = new StringBuilder();
        int joined = 0;
        for (Object o : list) {
            if (o != null) {
                if (!o.toString().isEmpty() || keepEmpty) {
                    if (joined > 0) sb.append(separator);
                    sb.append(o.toString());
                    joined++;
                }
            }
        }
        return sb.toString();
    }

    public static String Join(Object[] list, String separator, boolean keepEmpty){
        StringBuilder sb = new StringBuilder();
        int joined = 0;
        for (Object o : list) {
            if (o != null) {
                if (!o.toString().isEmpty() || keepEmpty) {
                    if (joined > 0) sb.append(separator);
                    sb.append(o.toString());
                    joined++;
                }
            }
        }
        return sb.toString();
    }

    public static String StringToBase64(String string) {
        return Base64.getEncoder().encodeToString(string.getBytes());
    }
    public static String Base64ToString(String base64){
        return new String(Base64.getDecoder().decode(base64.getBytes()));
    }

}
