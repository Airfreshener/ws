package main.java.com.rhino.common.utils;

import javafx.scene.web.WebEngine;
import netscape.javascript.JSException;

import java.io.File;
import java.util.Random;

/**
 * Created by Vasiliy on 27.06.2016
 */
public class WebUtils {

    private final static String TAG = WebUtils.class.getSimpleName();

    public static boolean ReplaceBlock(String url, String html, String selector, String script, int w, int h, WebEngine engine, String sitename){
        if (selector == null || selector.isEmpty()) return false;
        if( (url == null || url.isEmpty()) && (html == null || html.isEmpty()) ) return false;

        String blockText;
        String innerHTML, sizes, frameSizes;
        sizes = "width:' + w + 'px;height:' + h + 'px;";
        if (w == 0 || h == 0) {
            frameSizes = "width=\"' + w + '\" height=\"' + h + '\"";
        } else {
            //sizes = String.format("width:%dpx;height:%dpx;", w, h);
            frameSizes = String.format("width=\"%d\" height=\"%d\"", w, h);
        }
        String filePrefix = "file:///";
        if (url !=null && !url.isEmpty()) {
            if(url.matches("^[a-zA-Z]:[/\\\\].*") && new File(url).exists()){
                url = filePrefix + url.replaceAll("\\\\", "/");
            }
            blockText = String.format("<iframe src=\"%s\" %s style=\"margin:auto;border:0px;\"></iframe>", url, frameSizes);
        } else {
            blockText = html;
        }

        innerHTML = "'<div style=\"width:' + w + 'px;height:' + h + 'px;display:table;text-align:center;\">" +
                "<div style=\"display:table-cell;vertical-align:middle;" + sizes + "\">" + blockText + "</div></div>'";

        int scrollPosition = h/5 + new Random().nextInt(Math.max(h - h * 2/5, 1)); // random position between 0.2 and 0.8 of h
        String q = "var cumulativeOffset = function(element) { var top = 0, left = 0; do { top += element.offsetTop  || 0; element = element.offsetParent; } while(element); return top;};" +
                "var el = document.querySelector('" + selector + "');" +
                "if(el){" +
                "var w = el.offsetWidth;" +
                "var h = el.offsetHeight;" +
                "el.innerHTML = " + innerHTML + ";" +
                //"el.style.display = 'none';" +
                "var offset = cumulativeOffset(el);" +
                "" +
                "if( offset > " + scrollPosition + " ) {" +
                "window.scrollBy(0, offset - " + scrollPosition + ");" +
                "}" +
                "true;" +
                "} else {" +
                "false;" +
                "}";
        //Log.d(TAG, "script: " + q);


        boolean result;
        try {
            result = (Boolean) engine.executeScript(q);
        }catch (JSException e){
            Log.e(sitename, TAG, e);
            result = false;
        }
        
        return  result;
    }

    
    public static boolean RunScript(String script, WebEngine engine, String sitename){
        boolean result = true;
        if(script != null && !script.isEmpty()) {
            try {
                engine.executeScript(script);
                result = true;
            } catch (JSException e) {
                result = false;
                Log.e(sitename, TAG, e);
            }
        }
        return result;
    }
}
