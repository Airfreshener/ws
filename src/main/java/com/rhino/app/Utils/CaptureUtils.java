package main.java.com.rhino.app.Utils;

import main.java.com.rhino.common.controller.SqlController;
import main.java.com.rhino.common.model.Block;
import main.java.com.rhino.common.model.Campaign;
import main.java.com.rhino.common.model.Site;
import main.java.com.rhino.common.utils.Log;
import main.java.com.rhino.common.utils.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Vasiliy on 27.06.2016
 */
public class CaptureUtils {

    private final static String TAG = CaptureUtils.class.getSimpleName();
    private static final boolean DEBUG = false;

    public static boolean Capture(Site site){
        try {
            if(site == null){
                return false;
            }
            SqlController sql = SqlController.getInstance();
            Campaign camp = site.getCamp();
            String idCamp =  camp == null ? "-1" : String.valueOf(camp.getId());
            //String defaultPath = CaptureUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            String pathCommon = sql.getSetting("setting_capture_path", "");
            String pathCamp = camp == null || camp.getPath() == null ? "" : camp.getPath();
            String path = "";
            if(path.isEmpty())
                path = pathCamp;
            if(path.isEmpty())
                path = pathCommon;
            if(path.isEmpty() || site.getName().isEmpty())
                path = new File(CaptureUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile().getAbsolutePath();

            //String path = (camp == null ? sql.getSetting("default_path", defaultPath) : camp.getPath());
            Block block;
            if(camp == null)
                block = site.getBlock();
            else {
                block = sql.getCampSiteLinkBlock(camp.getId(), site.getId());
                if(block == null)
                    block = camp.getBlock();
                if(block == null)
                    block = site.getBlock();
            }
            String[] commands = {
                    "java.exe",
                    "-jar",
                    "Worker.jar",
                    //"\"" + site.getCamp().getId() + "\"",
                    "\"" + idCamp + "\"",
                    "\"" + path.replaceAll("\\\\", "/") + "\"",
                    "\"" + site.getId() + "\"",
                    "\"" + site.getName() + "\"",
                    "\"" + site.getUrl() + "\"",
                    "\"" + site.getSelector() + "\"",
                    "\"" + site.getHeightPicture() + "\"",
                    "\"" + site.getWidthPicture() + "\"",
                    "\"" + site.getSize().ordinal() + "\"",
                    "\"" + StringUtils.StringToBase64(site.getScript()) + "\"",
                    "\"" + block.getId() + "\"",
                    "\"" + block.getHeight() + "\"",
                    "\"" + block.getWidth() + "\"",
                    "\"" + block.getName() + "\"",
                    "\"" + block.getHtml() + "\"",
                    "\"" + block.getUrl() + "\"",
                    "-XX:-CreateMinidumpOnCrash",
                    "-XXnoJrDump",
                    "-XXdumpSize:none"
            };

            if(DEBUG)
                Log.d(site.getName(), TAG, "Before start worker. Commands: " + StringUtils.Join(commands, " ; ", true));

            Runtime rt = Runtime.getRuntime();

            Process process = rt.exec(commands);

            InputStream stdin = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(stdin);
            BufferedReader br = new BufferedReader(isr);
            String line, error = "";
            while ((line = br.readLine()) != null)
                error += line + "\n";
            int exitVal = process.waitFor();
            if(exitVal != 0)
                Log.d(site.getName(), TAG, "Failed execute: code = " + exitVal + ", output: \n" + error);
            if(DEBUG)
                Log.d(site.getName(), TAG, String.format("Task executed: %d (%s)", exitVal, site.getName()));
            return exitVal == 0;
        } catch (Exception e){
            Log.e(site.getName(), TAG, "execute capturing failed: " + e.getMessage());
            return false;
        }
    }
}
