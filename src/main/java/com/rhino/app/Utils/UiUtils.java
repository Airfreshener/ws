package main.java.com.rhino.app.Utils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasiliy on 04.02.2016
 */
public class UiUtils {

    private final static String TAG = UiUtils.class.getSimpleName();

    public static void infoBox(String infoMessage){
        box(Alert.AlertType.INFORMATION, infoMessage, "Information", "");
    }
    public static void warningBox(String infoMessage){
        box(Alert.AlertType.WARNING, infoMessage, "Warning", "");
    }
    public static void errorBox(String infoMessage){
        box(Alert.AlertType.ERROR, infoMessage, "Error", "");
    }
    private static void box(Alert.AlertType type, String infoMessage, String titleBar, String headerMessage){
        Alert alert = new Alert(type);
        //alert.setTitle(!titleBar.isEmpty()? titleBar : alert.getAlertType().name());
        //if(!headerMessage.isEmpty())
        //    alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);
        alert.showAndWait();
    }

    public enum BindType{ WIDTH, HEIGHT, WIDTH_AND_HEIGHT }

    private static class SizeChangeListener implements ChangeListener<Number> {

        private final Region child;
        private final BindType type;
        private final int[] ltrbPadding;

        public SizeChangeListener(Region child, BindType type, int[] ltrbPadding){
            this.child = child;
            this.type = type;
            this.ltrbPadding = ltrbPadding;
        }

        @Override public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
            if(type == BindType.WIDTH){
                child.setPrefWidth(newValue.doubleValue() - ltrbPadding[0] - ltrbPadding[2]);
            } else {
                child.setPrefHeight(newValue.doubleValue() - ltrbPadding[1] - ltrbPadding[3]);
            }
        }
    }

    public static  <T> void forceListRefreshOn(ListView<T> lsv) {
        int indexSelection = lsv.getSelectionModel().getSelectedIndex();
        ObservableList<T> items = lsv.<T>getItems();
        lsv.<T>setItems(null);
        lsv.<T>setItems(items);
        lsv.getSelectionModel().select(indexSelection);
    }

    public static  <T> void forceListRefreshOn(TableView<T> tv) {
        int indexSelection = tv.getSelectionModel().getSelectedIndex();
        ObservableList<T> items = tv.<T>getItems();
        List<T> list = new ArrayList<T>();
        for(T item : items) list.add(item);
        items.clear();
        for(T item: list) items.add(item);
        //FXCollections.copy(items, list);
        //tv.<T>setItems(null);
        //tv.<T>setItems(items);
        tv.getSelectionModel().select(indexSelection);
    }

    public static  <T> void forceListRefreshOn(ComboBox<T> cb) {
        int indexSelection = cb.getSelectionModel().getSelectedIndex();
        ObservableList<T> items = cb.<T>getItems();
        cb.<T>setItems(null);
        cb.<T>setItems(items);
        cb.getSelectionModel().select(indexSelection);
    }
    
    
    public static void bindSize(Pane parent, Control child){
        child.prefWidthProperty().bind(parent.widthProperty());
        child.prefHeightProperty().bind(parent.heightProperty());
    }
    
    public static  void bindSize(Pane parent, Pane child){
        child.prefWidthProperty().bind(parent.widthProperty());
        child.prefHeightProperty().bind(parent.heightProperty());
    }
    
}
