package main.java.com.rhino.app.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.java.com.rhino.common.model.Block;
import main.java.com.rhino.common.utils.Log;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Vasiliy on 06.11.2016
 */
public class SelectBlockController extends IController {

    private final static String TAG = SelectBlockController.class.getSimpleName();

    @FXML Button btnBlockSelectOk;
    @FXML Button btnBlockSelectCancel;
    @FXML TableView<  Block> tvBlocks;
    @FXML TableColumn<Block, Integer> tcBlockId;
    @FXML TableColumn<Block, String>  tcBlockName;
    @FXML TableColumn<Block, String>  tcBlockPath;
    private ObservableList<Block> blocks;
    private Block selected = null;
    private boolean canceled = false;

    @Override
    public void init(URL location, ResourceBundle resources) {
        btnBlockSelectOk.setOnAction(new OkListener());
        btnBlockSelectCancel.setOnAction(new CancelListener());
        tcBlockId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcBlockName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tcBlockPath.setCellValueFactory(new PropertyValueFactory<>("url"));
        tvBlocks.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null)
                selected = newValue;
        });
        Log.d(TAG, "init");
    }

    // чтобы не грузить уже загруженный из базы список блоков
    void setBlocks(ObservableList<Block> blocks){
        this.blocks = blocks;
        tvBlocks.setItems(this.blocks);
    }

    boolean isCanceled(){
        return canceled;
    }

    Block getSelectedBlock(){
        return selected;
    }

    private class OkListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Stage stage = (Stage) btnBlockSelectOk.getScene().getWindow();
            stage.close();
        }
    }

    private class CancelListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            selected = null;
            canceled = true;
            Stage stage = (Stage) btnBlockSelectCancel.getScene().getWindow();
            stage.close();
        }
    }

}
