package main.java.com.rhino.app.controller;

import javafx.application.Platform;
import javafx.concurrent.Task;
import main.java.com.rhino.app.Utils.CaptureUtils;
import main.java.com.rhino.common.controller.SqlController;
import main.java.com.rhino.common.listener.TaskStateListener;
import main.java.com.rhino.common.model.Campaign;
import main.java.com.rhino.common.model.Site;
import main.java.com.rhino.common.utils.Log;

import java.io.File;
import java.time.LocalDate;
import java.util.*;

/**
 * Created by Vasiliy on 28.02.2016
 */
class ServiceController {

    private final static boolean DEBUG = false;
    
    private final static String TAG = ServiceController.class.getSimpleName();
    /** кол-во секунд в минуте */
    private final static int SEC_IN_MIN = 60;
    /** кол-во секунд в часе */
    private final static int SEC_IN_HOUR = SEC_IN_MIN * SEC_IN_MIN;
    /** часы и минуты начала рабочего времени и конца рабочего времени */
    private final static int HOUR_START_TIME = 6, HOUR_END_TIME = 23, MIN_END_TIME = 30;
    /** минимвльный интервал между выполнениями в секундах */
    private final static int SEC_MINIMUM_INTERVAL = 60;
    /** кол-во секунд от начала суток до начала рабочего времени (6:00) */
    private final static int SEC_TIME_DAY_START = HOUR_START_TIME * SEC_IN_HOUR;
    /** кол-во секунд от начала суток до конца рабочего времени (23:30) */
    private final static int SEC_TIME_DAY_END = (HOUR_END_TIME * SEC_IN_MIN + MIN_END_TIME) * SEC_IN_MIN;
    
    
    private static List<Campaign> campaignsStarted = new ArrayList<>();
    private static Map<Site, SiteTask> tasks = new HashMap<>();
    private static Task taskReportsCleaner;
    static {
        taskReportsCleaner = new ReportsCleanerTask();
        new Thread(taskReportsCleaner).start();
    }

    static synchronized void StartTask(Campaign camp, TaskStateListener taskStateListener) {
        if(!campaignsStarted.contains(camp)) {
            for(Site site : camp.getSites()) {
                SiteTask task = new SiteTask(site, taskStateListener);
                new Thread(task).start();
                tasks.put(site, task);
            }
            campaignsStarted.add(camp);
        }
    }
    
    static synchronized void StopTask(final Campaign camp){
        if(campaignsStarted.contains(camp)){
            for(Site site : camp.getSites()) {
                Task task = tasks.get(site);
                task.cancel();
                tasks.remove(site);
                Log.d(TAG, "Campaign " + camp.getName() + " stopped. Site task " + site.getName() + " stopped.");
            }
            campaignsStarted.remove(camp);
            SaveCampaignsStates();
        }
    }
    
    static synchronized void SaveCampaignsStates(){
        List<Campaign> camps = campaignsStarted;
        String[] ids = new String[camps.size()];
        for (int i = 0; i < camps.size(); i++) ids[i] = String.valueOf(camps.get(i).getId());
        String active = String.join(";", ids);
        SqlController.getInstance().setSetting("active_tasks", active);
    }

    static synchronized void StopAllTasks(){
        Log.d(TAG, "All campaigns stopped. Tasks in progress: " + tasks.size());
        for(Task task : tasks.values()){
            task.cancel();
        }
        taskReportsCleaner.cancel();
        tasks.clear();
        campaignsStarted.clear();
    }

    static synchronized boolean IsTaskRunning(Campaign campaign){
        return campaignsStarted.contains(campaign);
    }

    static class ReportsCleanerTask extends Task<Void> {

        @Override
        protected Void call() throws Exception {
            while (true) {
                if (isCancelled()) {
                    break;
                }


                //delete all files with reports
                final String reg = "hs_err_pid";
                File dir = new File(".");
                File [] files = dir.listFiles((dir1, name) -> name.startsWith(reg));

                if(files != null)
                    for (File file : files) {
                        Log.d(TAG, "Delete crash report( " + file.getAbsolutePath() +  " ): " + file.delete());
                    }


                Thread.sleep(10 * 1000);
            }
            
            Log.d(TAG, "Reports task stopped.");
            return null;
        }
    }

    static class SiteTask extends Task<Void> {

        private final Site site;
        private final TaskStateListener taskStateListener;
        private final Calendar calCampaignEnd = GregorianCalendar.getInstance();
        private final Calendar calCampaignStart = GregorianCalendar.getInstance();
        private final Calendar calLastDay = GregorianCalendar.getInstance();
        /** оставшиеся стампы в течении дня */
        private final List<Integer> secStamps = new ArrayList<>();
        //private final Calendar CalTmpDate = GregorianCalendar.getInstance();
        /** время начала суток текущего дня в секундах*/
        private long secTodayMidNight;
        /** средний интервал между выполнениями в секундах */
        private final int secInterval;
        private int cntTodayMade = 0;


        SiteTask(Site site, TaskStateListener taskStateListener){
            this.site = site;
            this.taskStateListener = taskStateListener;
            LocalDate start = site.getCamp().getDateStart();
            LocalDate end = site.getCamp().getDateEnd();
            
            if(start != null) {
                calCampaignStart.set(Calendar.YEAR, start.getYear());
                calCampaignStart.set(Calendar.DAY_OF_YEAR, start.getDayOfYear());
            }
            calCampaignStart.set(Calendar.HOUR_OF_DAY, 0);
            calCampaignStart.set(Calendar.MINUTE, 0);
            calCampaignStart.set(Calendar.SECOND, 0);
            
            
            if(end != null){
                calCampaignEnd.set(Calendar.YEAR, end.getYear());
                calCampaignEnd.set(Calendar.DAY_OF_YEAR, end.getDayOfYear());
            }
            calCampaignEnd.set(Calendar.HOUR_OF_DAY, 0);
            calCampaignEnd.set(Calendar.MINUTE, 0);
            calCampaignEnd.set(Calendar.SECOND, 0);
            calCampaignEnd.add(Calendar.DATE, 1);
            
            calLastDay.set(Calendar.YEAR, calCampaignStart.get(Calendar.YEAR));
            calLastDay.set(Calendar.DAY_OF_YEAR, calCampaignStart.get(Calendar.DAY_OF_YEAR));
            calLastDay.set(Calendar.HOUR_OF_DAY, 0);
            calLastDay.set(Calendar.MINUTE, 0);
            calLastDay.set(Calendar.SECOND, 0);
    
            secInterval = Math.max((SEC_TIME_DAY_END - SEC_TIME_DAY_START) / site.getCountInDay(), SEC_MINIMUM_INTERVAL);
    
    
            updateStamps();
        }

        private void updateStamps(){
            
            Calendar cNow = GregorianCalendar.getInstance();
            long secNow = cNow.getTimeInMillis() / 1000;
            cNow.set(Calendar.HOUR_OF_DAY, 0);
            cNow.set(Calendar.MINUTE, 0);
            cNow.set(Calendar.SECOND, 0);
            secTodayMidNight = cNow.getTimeInMillis() / 1000;
            
            secStamps.clear();
            int secTime = SEC_TIME_DAY_START;
            int secNowInDay = (int) (secNow - secTodayMidNight);
            while(secTime < SEC_TIME_DAY_END){
                // рандомизация интервалов с ограничением минимального времени между выполнениями
                secTime += Math.max(secInterval/2 + new Random().nextInt(secInterval), SEC_MINIMUM_INTERVAL);
                // точное разбиение на интервалы
                //secTime += secInterval;
                if(secTime > secNowInDay) // добавляем в список только те стампы, что после текущего момента
                    secStamps.add(secTime);
            }
            
            Log.i(site.getName(), TAG, String.format(Locale.ENGLISH, "Stamps updated. Name: %s, stamps: %d, nearest: %s", site.getName(), secStamps.size(),
                    (secStamps.size() > 0 ? "after " + (secStamps.get(0) - secNowInDay) + " seconds": " tomorrow")));
        }

        @Override
        protected Void call() throws Exception {
            while (true){
                if (isCancelled())
                    break;

                try {
                    Calendar cNow = GregorianCalendar.getInstance();
                    if (cNow.after(calCampaignEnd)) { // проверка окончания кампании
                        campaignsStarted.remove(site.getCamp());
                        StopTask(site.getCamp());
                        Log.i(site.getName(), TAG, "Campaign " + site.getCamp().getName() + " ended!");
                        break;
                    }
    
                    // проверка на дату начала кампании
                    if(calCampaignStart == null || cNow.after(calCampaignStart)) {
                        int dayNow = cNow.get(Calendar.DAY_OF_YEAR);
                        
                        // проверка текущего дня с днем, для которого рассчита стампы
                        if (calLastDay.get(Calendar.DAY_OF_YEAR) != dayNow) {
                            Log.d(site.getName(), TAG, "The next day has come! Site: " + site.getId());
                            cntTodayMade = 0;
                            calLastDay.set(Calendar.DAY_OF_YEAR, dayNow);
                            calLastDay.set(Calendar.YEAR, cNow.get(Calendar.YEAR));
                            updateStamps();
                        }

                        //формальная проверка на кол-во выполнений в день
                        if (cntTodayMade < site.getCountInDay()) {
                            long secNow = cNow.getTimeInMillis() / 1000;
                            if (secStamps.size() > 0 && secNow - secTodayMidNight > secStamps.get(0)) {
                                secStamps.remove(0);
                                cntTodayMade++;
            
                                Log.d(site.getName(), TAG, String.format("Executing task: %s", site.getName()));
                                long t1 = System.currentTimeMillis();
                                boolean result = CaptureUtils.Capture(site);
                                int time = (int)((System.currentTimeMillis() - t1)/1000);
                                secNow = GregorianCalendar.getInstance().getTimeInMillis() / 1000;
                                long secNowInDay = secNow - secTodayMidNight;
                                if(DEBUG) {
                                    String nextStamp = (secStamps.size() > 0 ? "after " + (secStamps.get(0) - secNowInDay) + " sec" : " tomorrow");
                                    String log = String.format("Execute result: %b (%s), time: %d sec, next stamp: %s",
                                            result, site.getName(), time, nextStamp);
                                    Log.d(site.getName(), TAG, log);
                                }
    
                            }
                        }
                    }
                } catch(Exception e) {
                    Log.e(site.getName(), TAG, "Service task failed: " + e.getMessage());
                }

                Thread.sleep(5 * 1000);
            }

            Platform.runLater(() -> {
                try {
                    if (taskStateListener != null) {
                        taskStateListener.taskStopped(site);
                    }
                } catch (Exception e){
                    Log.e(site.getName(), TAG, e);
                }
            });

            Log.d(site.getName(), TAG, "Task stopped: id = " + site.getId() + ", name = " + site.getName());
            
            return null;
        }

    }

}
