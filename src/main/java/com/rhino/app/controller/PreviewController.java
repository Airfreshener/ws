package main.java.com.rhino.app.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import main.java.com.rhino.app.Utils.UiUtils;
import main.java.com.rhino.common.controller.SqlController;
import main.java.com.rhino.common.model.Block;
import main.java.com.rhino.common.model.Cookie;
import main.java.com.rhino.common.model.Site;
import main.java.com.rhino.common.utils.Const;
import main.java.com.rhino.common.utils.Log;
import main.java.com.rhino.common.utils.WebUtils;
import netscape.javascript.JSException;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vasiliy on 28.02.2016
 */
public class PreviewController {

    private final static String TAG = PreviewController.class.getSimpleName();
    private static Site site;
    private final static int IMAGE_WIDTH = 1500, IMAGE_HEIGHT = (int) (Screen.getPrimary().getVisualBounds().getHeight() - 50);
    private JFrame jFrame;
    private JFXPanel jfxPanel;
    private WebView wvPreviewer;
    private CookieManager cookieManager;
    
    PreviewController(){
        wvPreviewer = new WebView();
        jfxPanel = new JFXPanel();
        Scene scene = new Scene(wvPreviewer);
        jfxPanel.setScene(scene);
        jFrame = new JFrame("Preview");
        jFrame.setVisible(false);
        jFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                SqlController sql = SqlController.getInstance();
                List<Cookie> cookies = new ArrayList<>();
                List<HttpCookie> list = cookieManager.getCookieStore().getCookies();
                /*for(URI uri : cookieManager.getCookieStore().getURIs()) {
                    for(HttpCookie httpCookie : cookieManager.getCookieStore().get(uri)) {
                        System.out.println("test > " + uri.toASCIIString() + " # " + httpCookie.toString() + " - " + httpCookie.getSecure());
                    }
                }*/
                for (HttpCookie c : list){
                    Cookie cookie = new Cookie();
                    cookie.name = c.getName();
                    cookie.value = c.getValue();
                    cookie.site_id = site.getId();
                    cookies.add(cookie);
                }
                sql.setCookies(cookies, site.getId());
                //super.windowClosing(e);
                e.getWindow().dispose();
            }
        });

    }

    public void hide(){
        jFrame.setVisible(false);
    }
    
    void startPreview(Site s){
        site = s;
        jFrame.setVisible(true);
        jFrame.setTitle("Preview: " + site.getName());
        //jFrame.setLayout(new BoxLayout());

        int sizeW = site.getWidthPicture() == 0 ? IMAGE_WIDTH : site.getWidthPicture();
        int sizeH = IMAGE_HEIGHT;
        wvPreviewer.setPrefSize(sizeW, sizeH);
        jFrame.add(jfxPanel);
        jFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        jFrame.setSize(sizeW, sizeH);
        jfxPanel.setSize(sizeW, sizeH);

        WebEngine engine = wvPreviewer.getEngine();
        engine.getLoadWorker().stateProperty().addListener(new PreviewListener(wvPreviewer, site));
        //engine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            //JSObject window = (JSObject) engine.executeScript("window");
            //JavaBridge bridge = new JavaBridge();
            //window.setMember("java", bridge);
            //engine.executeScript("console.log = function(message) { java.log(message); };");
        //});
        engine.setUserAgent(Const.USER_AGENT); // fake it like google chrome
    
        try {
            cookieManager = new java.net.CookieManager();
            CookieHandler.setDefault(cookieManager);
            SqlController sql = SqlController.getInstance();
            List<String> cookies = sql.getCookies(site.getId());
            Map<String, List<String>> headers = new LinkedHashMap<>();
            headers.put("Set-Cookie", cookies);
            cookieManager.put(URI.create(site.getUrl()), headers);
        } catch (Exception e){
            e.printStackTrace();
        }
        
        
        engine.load(site.getUrl());
    }

/*
    public class JavaBridge {
        public void log(String text) {
            Log.d(TAG, "console: " + text);
        }
    }
*/
    
    void debug1(){
        try {
            JFXPanel panel = jfxPanel;
            WebView webView = wvPreviewer;
            WebEngine engine = wvPreviewer.getEngine();
            float height = (Integer)engine.executeScript("var body = document.body, html = document.documentElement; var height = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); height;");
            height = Math.min(height, 14000);
            engine.executeScript("window.scrollTo(0, 0);");
            wvPreviewer.getEngine().executeScript("var ce = null;" +
                    "$('body, body *').bind('mouseover', function(e) { if(e.target === e.currentTarget) ce = $(this).get(0); });\n"
                    //"$(this).off('contextmenu');\n" +
                    //"$(this).contextmenu(function(e) {\n" +
                    //    "var x = event.clientX, y = event.clientY, ce = document.elementFromPoint(x, y);\n" +
                    //"});"
            );
        
            panel.setSize(panel.getWidth(), (int) height);
            webView.resize(panel.getWidth(), (int) height);
            webView.setPrefSize(panel.getWidth(), (int) height);
            webView.setMinHeight(height);
            Log.d(site.getName(), TAG, "changed height: " + height);
        }catch (JSException e){
            Log.e(site.getName(), TAG, e);
        }
    
    }
    
    void debug2(){
        Object log = wvPreviewer.getEngine().executeScript(
                "if(ce){" +
                    //"console.log(ce);" +
                    "var str = 'el: ' + ce + ', '; " +
//                        "ce;" +

                    //"$(ce.attributes).each(function() {" +
                    //    "console.log(this.nodeName + ' : ' + this.nodeValue + '; ');" +
                    //"});" +
//                    "str += ', id: ' + ce.attr('id') + ', classes: ' + ce.className;" +

                    "var attributes = ce.attributes;" +
                    "var n = attributes.length;" +
                    "for (var i = 0; i < n; i++){\n" +
                        "str += attributes[i].nodeName + ' : ' + attributes[i].nodeValue + '; ';\n" +
                    "}" +

                    "str;" +
                "}");
        Log.d(TAG, log.toString());
    }
    
    void replace() {
        if(site != null) {
            Block block = site.getBlock();
            if (block != null) {
                String selector = site.getSelector();
                int w = block.getWidth(), h = block.getHeight();
                if (block.getUrl().isEmpty() && block.getHtml().isEmpty()) {
                    UiUtils.warningBox("Block data empty!");
                    return;
                }

                if (selector.isEmpty()) {
                    UiUtils.warningBox("Selector is empty!");
                    return;
                }
                WebUtils.ReplaceBlock(block.getUrl(), block.getHtml(), selector, site.getScript(), w, h, wvPreviewer.getEngine(), site.getName());
            } else {
                UiUtils.warningBox("Block not selected!");
            }
        }
    }

    private class PreviewListener implements ChangeListener<Worker.State> {

        private final WebEngine engine;
        private final Site site;

        PreviewListener(WebView webView, Site site){
            this.engine = webView.getEngine();
            this.site = site;
        }

        @Override public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
            Log.d(TAG, "Preview page (" + site.getName() + ") state changed: " + newValue.toString());
            if (engine.getLoadWorker().getException() != null && newValue == Worker.State.FAILED){
                Throwable e = engine.getLoadWorker().getException();
                String exceptionMessage = "Exception: " + e.toString();
                Log.e(TAG, exceptionMessage);
                Log.e(TAG, e);
            }
            if (newValue == Worker.State.SUCCEEDED) {
                engine.getLoadWorker().stateProperty().removeListener(this);
                Log.d(TAG, "Preview page loaded: " + site.getUrl());

                //String hText = engine.executeScript("window.getComputedStyle(document.body, null).getPropertyValue('height')").toString();
                //double height = Double.valueOf(hText.replace("px", ""));
                // webView.setPrefHeight(height);
                //Object obj = engine.executeScript("document.querySelectorAll('password');");

                //tfSiteSelector.setEditable(true);
                //TODO make changes before capture
                //String html = (String) _engine.executeScript("document.documentElement.outerHTML");
                //System.out.println(html);
                //System.out.println(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());

            } else if(newValue == Worker.State.FAILED){
                Log.e(TAG, String.format("Loading preview page failed! url=[%s]", site.getUrl()));
            }
        }

    }



}
