package main.java.com.rhino.app.controller;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.java.com.rhino.app.Utils.CaptureUtils;
import main.java.com.rhino.app.Utils.UiUtils;
import main.java.com.rhino.common.controller.SqlController;
import main.java.com.rhino.common.listener.TaskStateListener;
import main.java.com.rhino.common.model.Block;
import main.java.com.rhino.common.model.Campaign;
import main.java.com.rhino.common.model.Site;
import main.java.com.rhino.common.utils.Const;
import main.java.com.rhino.common.utils.Log;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;

@SuppressWarnings("MagicNumber")
public class ViewController extends IController {

    private final static String TAG = ViewController.class.getSimpleName();
    private final static boolean DEBUG = true;

    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private Stage stage = null;
    private SqlController sql = SqlController.getInstance();
    private ObservableList<Campaign> campaigns;
    private ObservableList<Site> sites, sitesFiltered = FXCollections.observableArrayList(new ArrayList<Site>());
    private ObservableList<Block> blocks, blocksFiltered = FXCollections.observableArrayList(new ArrayList<Block>());

    @FXML BorderPane bpRoot;
    @FXML TabPane tpTabs;
    @FXML Tab tabCamps;
    @FXML Tab tabSites;
    @FXML Tab tabBlocks;
    @FXML Tab tabSettings;

    //region == blocks ui elements ==
    @FXML Button btnBlockAdd;
    @FXML Button btnBlockSave;
    @FXML Button btnBlockDelete;
    //@FXML Button btnBlockPath;
    @FXML CheckBox cbBlockSearchPath;
    @FXML TextArea taBlockHtml;
    @FXML TextField tfBlockUrl;
    @FXML TextField tfBlockName;
    @FXML TextField tfBlockSizeW;
    @FXML TextField tfBlockSizeH;
    @FXML TextField tfBlockSearch;
    @FXML TableView<Block> tvBlocks;
    @FXML TableColumn<Block, Integer> tcBlockId;
    @FXML TableColumn<Block, String> tcBlockName;
    @FXML TableColumn<Block, String> tcBlockPath;

    //endregion == blocks ui elements ==

    //region == sites ui elements ==
    @FXML Button btnSiteAdd;
    @FXML Button btnSiteDelete;
    @FXML Button btnSiteSave;
    @FXML Button btnSiteClearCookies;
    @FXML Button btnSiteCapture;
    @FXML Button btnSiteReplace;
    @FXML Button btnSiteDebug1;
    @FXML Button btnSiteDebug2;
    @FXML Button btnSiteLoad;
    @FXML TextArea taSiteScript;
    @FXML TextField tfSiteSelector;
    @FXML TextField tfSiteUrl;
    @FXML TextField tfSiteName;
    @FXML TextField tfSiteSizeW;
    @FXML TextField tfSiteSizeH;
    @FXML TextField tfSiteCountInDay;
    @FXML TextField tfSiteSearch;
    @FXML CheckBox cbSiteSearchUrl;
    @FXML ComboBox<Block> cbSiteBlock;
    @FXML TableView<Site> tvSites;
    @FXML TableColumn<Site, Integer> tcSitesId;
    @FXML TableColumn<Site, String> tcSitesName;
    @FXML TableColumn<Site, String> tcSitesUrl;
    @FXML TableColumn<Site, String> tcSitesBlock;

    //endregion == sites ui elements ==

    //region ==campaigns ui elements ==
    @FXML Button btnCampStart;
    @FXML Button btnCampStop;
    @FXML Button btnCampAdd;
    @FXML Button btnCampDelete;
    @FXML Button btnCampSave;
    @FXML Button btnCampPath;
    @FXML Button btnCampSiteAdd;
    @FXML Button btnCampSiteDelete;
    @FXML Button btnCampBlockChange;
    @FXML Button btnCampSiteBlockChange;
    @FXML TextField tfCampPath;
    @FXML TextField tfCampName;
    @FXML TextField tfCampBlock;
    @FXML TextField tfCampSiteBlock;
    @FXML DatePicker dpCampEndDate;
    @FXML DatePicker dpCampStartDate;
    @FXML TableView<Site> tvCampSites;
    @FXML TableView<Campaign> tvCamps;
    @FXML TableColumn<Campaign, Integer> tcCampId;
    @FXML TableColumn<Campaign, String> tcCampName;
    @FXML TableColumn<Campaign, String> tcCampSites;
    @FXML TableColumn<Campaign, String> tcCampPath;
    @FXML TableColumn<Campaign, String> tcCampDateStart;
    @FXML TableColumn<Campaign, String> tcCampDateEnd;
    @FXML TableColumn<Campaign, String> tcCampStatus;
    @FXML TableColumn<Site, Integer> tcCampSitesId;
    @FXML TableColumn<Site, String> tcCampSitesName;
    @FXML TableColumn<Site, String> tcCampSitesUrl;
    @FXML TableColumn<Site, String> tcCampSitesBlock;
    //endregion ==campaigns ui elements ==

    //region == settings elements ==
    @FXML TextField teSettingsPath;
    @FXML Button btnSettingsSave;
    @FXML Button btnSettingsCancel;
    @FXML Button btnSettingsPathChange;
    //endregion == settings elements ==

    private PreviewController pc;

    @Override public void init(URL location, ResourceBundle resources) {
        //spPreview.setStyle("-fx-background-color:blue; -fx-border-color:crimson;");
        pc = new PreviewController();
    
        blocks = sql.getBlocks();
        
        sites = sql.getSites();
        for(Site site : sites) site.setBlock(blocks);
        
        campaigns = sql.getCamps();
        for(Campaign camp : campaigns) {
            camp.setBlock(blocks);
            camp.setSites(sites);
        }

        initBlocksTab();
        initSitesTab();
        initCampsTab();
        initSettingsTab();

        //restore saved state
        String active = sql.getSetting("active_tasks", "");
        String[] ids = active.split(";");
        for (String id : ids) {
            for (Campaign camp : campaigns) {
                if (id.equals(String.valueOf(camp.getId()))) {
                    ServiceController.StartTask(camp, new TaskFinishListener());
                    break;
                }
            }
        }
        UiUtils.forceListRefreshOn(tvCamps);

        if(DEBUG){
            tcCampId.setVisible(true);
            tcSitesId.setVisible(true);
            tcBlockId.setVisible(true);
        }

        Log.d(TAG, "Application prepared!");
    }

    private void initBlocksTab(){
        updateBlocksFilter();
        btnBlockAdd.setOnAction(new AddBlockClickListener());
        btnBlockSave.setOnAction(new SaveBlockClickListener());
        btnBlockDelete.setOnAction(new DeleteBlockClickListener());
        btnBlockDelete.setDisable(blocksFiltered.size() == 0);
        btnBlockSave.setDisable(blocksFiltered.size() == 0);
        //btnBlockPath.setOnAction(new PathBlockListener());
        tvBlocks.setItems(blocksFiltered);
        tcBlockId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcBlockName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tcBlockPath.setCellValueFactory(new PropertyValueFactory<>("url"));
        tfBlockSearch.textProperty().addListener((observable, oldValue, newValue) -> updateBlocksFilter());
        cbBlockSearchPath.setSelected(sql.getSetting(Const.SEARCH_BLOCK_IN_PATH, false));
        cbBlockSearchPath.setOnAction(event -> {
            Boolean is = cbBlockSearchPath.isSelected();
            sql.setSetting(Const.SEARCH_BLOCK_IN_PATH, is.toString());
            updateBlocksFilter();
        });
        blocks.addListener((ListChangeListener<Block>) change -> {
            btnBlockDelete.setDisable(blocksFiltered.size() == 0);
            btnBlockSave.setDisable(blocksFiltered.size() == 0);
            updateBlocksFilter();
        });
        tvBlocks.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue == null) return;
            tfBlockName.setText(newValue.getName());
            tfBlockUrl.setText(newValue.getUrl());
            taBlockHtml.setText(newValue.getHtml());
            tfBlockSizeH.setText(String.valueOf(newValue.getHeight()));
            tfBlockSizeW.setText(String.valueOf(newValue.getWidth()));
        });
        btnBlockDelete.setDisable(blocksFiltered.size() == 0);
        if(blocksFiltered.size() > 0)
            tvBlocks.getSelectionModel().selectFirst();
    }

    private void initSitesTab(){
        updateSitesFilter();
        btnSiteCapture.setOnAction(new CaptureClickListener());
        btnSiteReplace.setOnAction(new ReplaceClickListener());
        btnSiteDebug1.setOnAction(new Debug1ClickListener());
        btnSiteDebug2.setOnAction(new Debug2ClickListener());
        btnSiteLoad.setOnAction(new LoadClickListener());
        btnSiteAdd.setOnAction(new AddSiteClickListener());
        btnSiteDelete.setOnAction(new DeleteSiteClickListener());
        btnSiteSave.setOnAction(new SaveSiteClickListener());
        btnSiteClearCookies.setOnAction(new ClearCookiesSiteClickListener());
        tcSitesId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcSitesName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tcSitesUrl.setCellValueFactory(new PropertyValueFactory<>("url"));
        tcSitesBlock.setCellValueFactory(data -> {
            String blockName = data.getValue() != null && data.getValue().getBlock() != null &&data.getValue().getBlock().getName() != null
                    ? data.getValue().getBlock().getName() : "";
            return new SimpleStringProperty(blockName);
        });
        tfSiteSearch.textProperty().addListener((observable, oldValue, newValue) -> updateSitesFilter());
        cbSiteSearchUrl.setSelected(sql.getSetting(Const.SEARCH_SITE_IN_URL, false));
        cbSiteSearchUrl.setOnAction(event -> {
            Boolean is = cbSiteSearchUrl.isSelected();
            sql.setSetting(Const.SEARCH_SITE_IN_URL, is.toString());
            updateSitesFilter();
        });

        btnSiteDelete.setDisable(sitesFiltered.size() == 0);
        btnSiteSave.setDisable(sitesFiltered.size() == 0);
        tvSites.setItems(sitesFiltered);
        sites.addListener((ListChangeListener<Site>) c -> {
            btnSiteDelete.setDisable(sitesFiltered.size() == 0);
            btnSiteSave.setDisable(sitesFiltered.size() == 0);
            updateSitesFilter();
        });
        tvSites.getSelectionModel().selectedItemProperty().addListener(new SiteSelectionListener());
        cbSiteBlock.setItems(blocks);
        cbSiteBlock.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
        });
        btnSiteDelete.setDisable(sitesFiltered.size() == 0);
        if(sitesFiltered.size() > 0)
            tvSites.getSelectionModel().selectFirst();
    }

    private void initCampsTab(){
        btnCampAdd.setOnAction(new AddCampClickListener());
        btnCampDelete.setOnAction(new DeleteCampClickListener());
        btnCampSave.setOnAction(new SaveCampClickListener());
        btnCampPath.setOnAction(new PathCampClickListener());
        btnCampStart.setOnAction(new StartClickListener());
        btnCampStop.setOnAction(new StopClickListener());
        btnCampSiteAdd.setOnAction(new CampSiteAddListener());
        btnCampSiteDelete.setOnAction(new CampSiteDeleteListener());
        btnCampBlockChange.setOnAction(new CampBlockChangeListener());
        btnCampSiteBlockChange.setOnAction(new CampSiteBlockChangeListener());
        dpCampEndDate.setOnAction(new EndDateClickListener());
        dpCampStartDate.setOnAction(new StartDateClickListener());
        tcCampId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcCampName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tcCampPath.setCellValueFactory(new PropertyValueFactory<>("path"));
        tcCampSites.setCellValueFactory(data -> new SimpleStringProperty(String.valueOf(data.getValue().getSites().size())));
        tcCampDateEnd.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getDateEndStr()));
        tcCampDateStart.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getDateStartStr()));
        tcCampStatus.setCellValueFactory(data -> {
            Campaign camp = data.getValue();
            String status = getStatusForCamp(camp);
            return new SimpleStringProperty(status);
        });

        btnCampDelete.setDisable(campaigns.size() == 0);
        btnCampSave.setDisable(campaigns.size() == 0);
        tvCamps.setItems(campaigns);
        campaigns.addListener((ListChangeListener<Campaign>) c -> {
            btnCampDelete.setDisable(campaigns.size() == 0);
            btnCampSave.setDisable(campaigns.size() == 0);
        });
        tcCampSitesId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcCampSitesName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tcCampSitesUrl.setCellValueFactory(new PropertyValueFactory<>("url"));
        tcCampSitesBlock.setCellValueFactory(param -> {
            Campaign c = tvCamps.getSelectionModel().getSelectedItem();
            Site s = param.getValue();
            Block bCampSite = sql.getCampSiteLinkBlock(c.getId(), s.getId());
            Block bCamp = c.getBlock();
            Block bSite = s.getBlock();
            String blockName = "";
            if(bCampSite != null)
                blockName = bCampSite.getName();
            else if(bCamp != null)
                blockName = bCamp.getName();
            else if(bSite != null)
                blockName = bSite.getName();
            return new SimpleStringProperty(blockName);
        });
        tvCampSites.setRowFactory(param -> {
            final TableRow<Site> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Site site = row.getItem();
                    tpTabs.getSelectionModel().select(tabSites);
                    for(Site s : sites){
                        if(s.getId() == site.getId()){
                            tvSites.getSelectionModel().select(s);
                            break;
                        }
                    }
                    //UiUtils.forceListRefreshOn(tvSites);
                }
            });
            return row ;
        });

        tvCamps.getSelectionModel().selectedItemProperty().addListener(new CampSelectionListener());
        tvCampSites.getSelectionModel().selectedItemProperty().addListener(new CampSiteSelectionListener());

        if(campaigns.size() > 0)
            tvCamps.getSelectionModel().selectFirst();
    }

    private void initSettingsTab(){
        btnSettingsSave.setOnAction(new SaveSettingsClickListener());
        btnSettingsCancel.setOnAction(new CancelSettingsClickListener());
        btnSettingsPathChange.setOnAction(new PathChangeSettingsClickListener());
        loadSettings();
    }

    public void setScene(Stage stage) {
        this.stage = stage;
    }

    public void onCloseRequest(){
        //stop statistic thread
        //taskStatistic.cancel();
        ServiceController.SaveCampaignsStates();
        ServiceController.StopAllTasks();
    }

    private void loadSettings(){
        teSettingsPath.setText(sql.getSetting("setting_capture_path", ""));
    }

    private String getStatusForCamp(Campaign camp){
        boolean isRun = ServiceController.IsTaskRunning(camp);
        Calendar cNow = GregorianCalendar.getInstance();
        Calendar calCampaignEnd = GregorianCalendar.getInstance();
        Calendar calCampaignStart = GregorianCalendar.getInstance();
        LocalDate start = camp.getDateStart();
        LocalDate end = camp.getDateEnd();
        if(start != null) {
            calCampaignStart.set(Calendar.YEAR, start.getYear());
            calCampaignStart.set(Calendar.DAY_OF_YEAR, start.getDayOfYear());
        }
        calCampaignStart.set(Calendar.HOUR, 0);
        calCampaignStart.set(Calendar.MINUTE, 0);
        calCampaignStart.set(Calendar.SECOND, 0);
        if(end != null){
            calCampaignEnd.set(Calendar.YEAR, end.getYear());
            calCampaignEnd.set(Calendar.DAY_OF_YEAR, end.getDayOfYear());
        }
        calCampaignEnd.set(Calendar.HOUR, 0);
        calCampaignEnd.set(Calendar.MINUTE, 0);
        calCampaignEnd.set(Calendar.SECOND, 0);
        calCampaignEnd.add(Calendar.DATE, 1);
        String status;
        if(isRun) {
            status = "Running";
        } else if (cNow.after(calCampaignEnd)){
            status = "Ended";
        } else if(cNow.before(calCampaignStart)){
            status = "Scheduled";
        } else {
            status = "Stopped";
        }
        return status;
    }

    private void updateSitesFilter(){
        String query = tfSiteSearch.getText();
        if(query == null || query.isEmpty()){
            sitesFiltered.clear();
            sitesFiltered.addAll(sites);
        } else {
            String q = query.toLowerCase();
            sitesFiltered.clear();
            Collection<Site> filtered = new ArrayList<>();
            boolean isUrl = sql.getSetting(Const.SEARCH_SITE_IN_URL, false);
            for(Site s : sites){
                if(s.getName().toLowerCase().contains(q) || (isUrl && s.getUrl().toLowerCase().contains(q))){
                    filtered.add(s);
                }
            }
            sitesFiltered.addAll(filtered);
        }
        //UiUtils.forceListRefreshOn(tvSites);
        tvSites.setItems(sitesFiltered);
    }

    private void updateBlocksFilter(){
        String query = tfBlockSearch.getText();
        if(query == null || query.isEmpty()){
            blocksFiltered.clear();
            blocksFiltered.addAll(blocks);
        } else {
            String q = query.toLowerCase();
            blocksFiltered.clear();
            Collection<Block> filtered = new ArrayList<>();
            boolean isPath = sql.getSetting(Const.SEARCH_BLOCK_IN_PATH, false);
            for(Block b : blocks){
                if(b.getName().toLowerCase().contains(q) || (isPath && b.getUrl().toLowerCase().contains(q))){
                    filtered.add(b);
                }
            }
            blocksFiltered.addAll(filtered);
        }
        tvBlocks.setItems(blocksFiltered);
    }

    private class TaskFinishListener implements TaskStateListener {

        @Override public void taskStopped(Site site) {
            UiUtils.forceListRefreshOn(tvCamps);
        }
    }

    //region === block tab actions ===
    private class AddBlockClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Block block = new Block(sql.getNextBlockId());
            sql.addBlock(block);
            blocks.add(block);
            tvBlocks.getSelectionModel().select(block);
        }
    }

    private class DeleteBlockClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Block block = tvBlocks.getSelectionModel().getSelectedItem();
            if(block != null){
                sql.deleteBlock(block.getId());
                blocks.remove(block);
                tvBlocks.getSelectionModel().selectLast();
                UiUtils.forceListRefreshOn(cbSiteBlock);
            }
        }
    }

    private class SaveBlockClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Block block = tvBlocks.getSelectionModel().getSelectedItem();
            if(block != null) {
                int w = 0;
                int h = 0;
                try {
                    if(tfBlockSizeW.getText().length() > 0) w = Integer.parseInt(tfBlockSizeW.getText());
                    if(tfBlockSizeH.getText().length() > 0) h = Integer.parseInt(tfBlockSizeH.getText());
                } catch (NumberFormatException e){
                    UiUtils.warningBox("Block size not valid");
                    return;
                }
                block.setName(tfBlockName.getText());
                block.setUrl(tfBlockUrl.getText());
                block.setHeight(h);
                block.setWidth(w);
                block.setHtml(taBlockHtml.getText());
                sql.updateBlock(block);
                UiUtils.forceListRefreshOn(tvBlocks);
                UiUtils.forceListRefreshOn(cbSiteBlock);
                Log.d(TAG, "Block updated: " + block.getName());
            }
        }
    }

/*
    private class PathBlockListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showOpenDialog(stage.getScene().getWindow());
            if(file != null && file.exists()){
                tfBlockUrl.setText(file.getAbsolutePath());
            }
        }
    }
*/

    //endregion === block tab actions ===

    //region === site tab actions ===
    private class AddSiteClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Site site = new Site(sql.getNextSiteId(), null);
            sql.addSite(site);
            sites.add(site);
            tfSiteSearch.setText(""); // reset filter
            //updateSitesFilter();
            tvSites.getSelectionModel().select(site);
        }
    }

    private class DeleteSiteClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            //Site site = lvSites.getSelectionModel().getSelectedItem();
            Site site = tvSites.getSelectionModel().getSelectedItem();
            if(site != null){
                sql.deleteSite(site.getId());
                sites.remove(site);
                tfSiteSearch.setText("");
                //updateSitesFilter();
                tvSites.getSelectionModel().selectLast();
            }
        }
    }

    private class SaveSiteClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            //Site site = lvSites.getSelectionModel().getSelectedItem();
            Site site = tvSites.getSelectionModel().getSelectedItem();
            if(site != null){
                int w;
                int h;
                try{
                    w = tfSiteSizeW.getText().length() > 0 ? Integer.parseInt(tfSiteSizeW.getText()) : 0;
                    h = tfSiteSizeH.getText().length() > 0 ? Integer.parseInt(tfSiteSizeH.getText()) : 0;
                } catch (NumberFormatException e){
                    UiUtils.warningBox("Picture size not valid");
                    return;
                }
                int count;
                try{
                    count = tfSiteCountInDay.getText().length() > 0 ? Integer.parseInt(tfSiteCountInDay.getText()) : 0;
                } catch (NumberFormatException e){
                    UiUtils.warningBox("Count times in day not valid");
                    return;
                }

                site.setScript(taSiteScript.getText());
                site.setName(tfSiteName.getText());
                site.setUrl(tfSiteUrl.getText());
                site.setHeightPicture(h);
                site.setWidthPicture(w);
                site.setSelector(tfSiteSelector.getText());
                Block block = cbSiteBlock.getSelectionModel().getSelectedItem();
                site.setBlockId(block == null ? -1 : block.getId());
                site.setBlock(block);
                site.setCountInDay(count);
                sql.updateSite(site);
                UiUtils.forceListRefreshOn(tvSites);

                for(Campaign camp : campaigns){
                    for(Site s : camp.getSites()){
                        if(s.getId() == site.getId())
                            s.update(site);
                    }
                }

                Log.d(TAG, "Site updated: " + site.getName());
            }
        }
    }

    private class ClearCookiesSiteClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Site site = tvSites.getSelectionModel().getSelectedItem();
            if(site != null){
                sql.setCookies(new ArrayList<>(), site.getId());
                UiUtils.forceListRefreshOn(tvSites);
                Log.d(TAG, "Site cookies clear: " + site.getName());
            }
        }
    }

    private class CaptureClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            //final Site site = lvSites.getSelectionModel().getSelectedItem();
            final Site site = tvSites.getSelectionModel().getSelectedItem();
            if(site == null){
                UiUtils.warningBox("Site not selected!");
                return;
            }

            new Thread(() -> {
                final boolean result = CaptureUtils.Capture(site);
                Platform.runLater(() -> UiUtils.infoBox("Capture done: " + (result ? "success" : "failed") + "!"));
            }).start();
/*
            new CaptureController().Capture(site, new TaskExecuteListener() {
                @Override
                public void executeFinished(boolean result) {
                    UiUtils.infoBox("Capture done: " + (result ? "success" : "failed") + "!");
                }
            });
*/
        }
    }

    private class ReplaceClickListener implements EventHandler<ActionEvent> {

        ReplaceClickListener(){}

        @Override public void handle(ActionEvent event) {
            pc.replace();
        }
    }
    
    private class Debug1ClickListener implements EventHandler<ActionEvent> {
    
        Debug1ClickListener(){}

        @Override public void handle(ActionEvent event) {
            pc.debug1();
        }
    }
    private class Debug2ClickListener implements EventHandler<ActionEvent> {
    
        Debug2ClickListener(){}

        @Override public void handle(ActionEvent event) {
            pc.debug2();
        }
    }

    private class LoadClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            //pc.startPreview(lvSites.getSelectionModel().getSelectedItem());
            pc.startPreview(tvSites.getSelectionModel().getSelectedItem());
            bpRoot.toFront();
        }
    }

    private class SiteSelectionListener implements ChangeListener<Site> {
        @Override public void changed(ObservableValue<? extends Site> observable, Site oldValue, Site newValue) {
            if (newValue != null) {
                taSiteScript.setText(newValue.getScript());
                tfSiteName.setText(newValue.getName());
                tfSiteUrl.setText(newValue.getUrl());
                tfSiteSelector.setText(newValue.getSelector());
                tfSiteSizeH.setText(String.valueOf(newValue.getHeightPicture()));
                tfSiteSizeW.setText(String.valueOf(newValue.getWidthPicture()));
                cbSiteBlock.getSelectionModel().select(newValue.getBlock());
                tfSiteCountInDay.setText(String.valueOf(newValue.getCountInDay()));
            }
        }
    }


    //endregion === site tab actions ===

    //region === camp tab actions ===
    private class AddCampClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Campaign camp = new Campaign(sql.getNextCampId());
            sql.addCamp(camp);
            campaigns.add(camp);
            tvCamps.getSelectionModel().select(camp);
        }
    }

    private class DeleteCampClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Campaign camp = tvCamps.getSelectionModel().getSelectedItem();
            if(camp != null){
                sql.deleteCamp(camp.getId());
                campaigns.remove(camp);
                tvCamps.getSelectionModel().selectLast();
            }
        }
    }

    private class SaveCampClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Campaign camp = tvCamps.getSelectionModel().getSelectedItem();
            if(camp != null){

                camp.setName(tfCampName.getText());
                camp.setPath(tfCampPath.getText());
                camp.setDateStart(dpCampStartDate.getValue());
                camp.setDateEnd(dpCampEndDate.getValue());
                //camp.setBlock(cbCampBlock.getSelectionModel().getSelectedItem());
                sql.updateCamp(camp);
                UiUtils.forceListRefreshOn(tvCamps);
                UiUtils.forceListRefreshOn(tvCampSites);
                Log.d(TAG, "Campaign updated: " + camp.getName());
            }
        }
    }

    private class CampSiteAddListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {

            Campaign camp = tvCamps.getSelectionModel().getSelectedItem();
            try {
                Stage stage = new Stage();
                ClassLoader resLoader = getClass().getClassLoader();
                URL xml = resLoader.getResource("view/add_site.fxml");
                FXMLLoader xmlLoader = new FXMLLoader(xml);
                Parent root = xmlLoader.load();
                final AddSiteController addSiteController = xmlLoader.getController();
                addSiteController.setCamp(camp);
                stage.setScene(new Scene(root));
                stage.setTitle("Add sites to campaign: " + (camp.getName().isEmpty() ? "unnamed" : camp.getName()));
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(((Node) event.getSource()).getScene().getWindow());
                stage.setMinHeight(300);
                stage.setMinWidth(500);
                stage.showAndWait();

            } catch (IOException e){
                e.printStackTrace();
                UiUtils.errorBox("Can not create dialog window");
            }


            UiUtils.forceListRefreshOn(tvCamps);

            /*camp.getSites().add(site);
            sql.updateCamp(camp);
            tvCamps.getSelectionModel().select(camp);
            UiUtils.forceListRefreshOn(tvCamps);*/
        }
    }

    private class CampSiteDeleteListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Campaign camp = tvCamps.getSelectionModel().getSelectedItem();
            Site site = tvCampSites.getSelectionModel().getSelectedItem();
            camp.getSites().remove(site);
            sql.updateCamp(camp);
            UiUtils.forceListRefreshOn(tvCamps);
        }
    }

    private class CampBlockChangeListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            try {
                Campaign camp = tvCamps.getSelectionModel().getSelectedItem();
                Stage stage = new Stage();
                ClassLoader resLoader = getClass().getClassLoader();
                URL xml = resLoader.getResource("view/select_block.fxml");
                FXMLLoader xmlLoader = new FXMLLoader(xml);
                Parent root = xmlLoader.load();
                final SelectBlockController controller = xmlLoader.getController();
                controller.setBlocks(blocks);
                stage.setScene(new Scene(root));
                stage.setTitle("Select block for campaign: " + (camp.getName().isEmpty() ? "unnamed" : camp.getName()));
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(((Node) event.getSource()).getScene().getWindow());
                stage.setMinHeight(300);
                stage.setMinWidth(500);
                stage.showAndWait();

                if(!controller.isCanceled()){
                    camp.setBlock(controller.getSelectedBlock());
                    sql.updateCamp(camp);
                }
            } catch (IOException e){
                e.printStackTrace();
                UiUtils.errorBox("Can not create dialog window");
            }


            UiUtils.forceListRefreshOn(tvCamps);

        }
    }

    private class CampSiteBlockChangeListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
                Campaign camp = tvCamps.getSelectionModel().getSelectedItem();
                Site site = tvCampSites.getSelectionModel().getSelectedItem();
                if(camp != null && site != null) {
                    try {
                        Stage stage = new Stage();
                        ClassLoader resLoader = getClass().getClassLoader();
                        URL xml = resLoader.getResource("view/select_block.fxml");
                        FXMLLoader xmlLoader = new FXMLLoader(xml);
                        Parent root = xmlLoader.load();
                        final SelectBlockController controller = xmlLoader.getController();
                        controller.setBlocks(blocks);
                        stage.setScene(new Scene(root));
                        stage.setTitle("Select block for site of campaign: " + (camp.getName().isEmpty() ? "unnamed" : camp.getName()));
                        stage.initModality(Modality.WINDOW_MODAL);
                        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
                        stage.setMinHeight(300);
                        stage.setMinWidth(500);
                        stage.showAndWait();

                        if (!controller.isCanceled()) {
                            if(controller.getSelectedBlock() != null)
                                sql.setCampSiteLink(camp.getId(), site.getId(),controller.getSelectedBlock().getId());
                            else
                                sql.deleteCampSiteLink(camp.getId(), site.getId());
                        }
                    } catch (IOException e){
                        e.printStackTrace();
                        UiUtils.errorBox("Can not create dialog window");
                    }

                    UiUtils.forceListRefreshOn(tvCampSites);
                }

        }
    }

    private class PathCampClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File dir = directoryChooser.showDialog(stage.getScene().getWindow());

            if(dir != null){
                try {
                    if (!dir.exists()) {
                        if (dir.mkdirs())
                            Log.d(TAG, "Created output camp directory: " + dir.getAbsolutePath());
                    }
                } catch (SecurityException e){
                    Log.e(TAG, "Can not create dir: " + e.getMessage());
                    return;
                }
                tfCampPath.setText(dir.getAbsolutePath());
            }
        }
    }

    private class EndDateClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            //LocalDate date = dpCampEndDate.getValue();
            //System.err.println("Selected end date: " + date);
        }
    }

    private class StartDateClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            //LocalDate date = dpCampStartDate.getValue();
            //System.err.println("Selected start date: " + date);
        }
    }

    private class StartClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            //Site site = lvSites.getSelectionModel().getSelectedItem();
            Campaign camp= tvCamps.getSelectionModel().getSelectedItem();
            ServiceController.StartTask(camp, new TaskFinishListener());
            ServiceController.SaveCampaignsStates();
            Log.d(TAG, "Campaign start: " + camp.getName());
            btnCampStart.setDisable(true);
            btnCampStop.setDisable(false);
            btnCampDelete.setDisable(true);
            //UiUtils.forceListRefreshOn(lvSites);
            UiUtils.forceListRefreshOn(tvCamps);
        }
    }

    private class StopClickListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            //Site site = lvSites.getSelectionModel().getSelectedItem();
            Campaign camp = tvCamps.getSelectionModel().getSelectedItem();
            ServiceController.StopTask(camp);
            Log.d(TAG, "Campaign stop: " + camp.getName());
            btnCampStart.setDisable(false);
            btnCampStop.setDisable(true);
            btnCampDelete.setDisable(false);
            UiUtils.forceListRefreshOn(tvCamps);
        }
    }

    private class CampSelectionListener implements ChangeListener<Campaign> {
        @Override public void changed(ObservableValue<? extends Campaign> observable, Campaign oldValue, Campaign newValue) {
            if(newValue != null) {
                tvCampSites.setItems(newValue.getSites());
                tfCampPath.setText(newValue.getPath());
                tfCampName.setText(newValue.getName());
                btnCampDelete.setDisable(ServiceController.IsTaskRunning(newValue));
                dpCampStartDate.setValue(newValue.getDateStart());
                dpCampEndDate.setValue(newValue.getDateEnd());
                btnCampStart.setDisable(ServiceController.IsTaskRunning(newValue));
                btnCampStop.setDisable(!ServiceController.IsTaskRunning(newValue));
                tfCampBlock.setText(newValue.getBlock() == null ? "" : newValue.getBlock().getName());
            }
        }
    }

    private class CampSiteSelectionListener implements ChangeListener<Site> {
        @Override public void changed(ObservableValue<? extends Site> observable, Site oldValue, Site newValue) {
            if(newValue != null) {
                Campaign c = tvCamps.getSelectionModel().getSelectedItem();
                Block b = sql.getCampSiteLinkBlock(c.getId(), newValue.getId());
                tfCampSiteBlock.setText(b == null ? "" : b.getName());
            }
        }
    }
    //endregion === camp tab actions ===


    //region === settings tab actions ===
    private class SaveSettingsClickListener implements EventHandler<ActionEvent>{
        @Override public void handle(ActionEvent event){
            String path = teSettingsPath.getText();
            sql.setSetting("setting_capture_path", path);
        }
    }

    private class CancelSettingsClickListener implements EventHandler<ActionEvent>{
        @Override public void handle(ActionEvent event){
            loadSettings();
        }
    }

    private class PathChangeSettingsClickListener implements EventHandler<ActionEvent>{
        @Override public void handle(ActionEvent event){
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File dir = directoryChooser.showDialog(stage.getScene().getWindow());

            if(dir != null){
                try {
                    if (!dir.exists()) {
                        if (dir.mkdirs())
                            Log.d(TAG, "Created output camp directory: " + dir.getAbsolutePath());
                    }
                } catch (SecurityException e){
                    Log.e(TAG, "Can not create dir: " + e.getMessage());
                    return;
                }
                sql.setSetting("setting_capture_path", dir.getAbsolutePath());
                teSettingsPath.setText(dir.getAbsolutePath());
            }
        }
    }
    //endregion === settings tab actions ===
}
