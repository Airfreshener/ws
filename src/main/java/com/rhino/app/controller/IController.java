package main.java.com.rhino.app.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Vasiliy on 06.11.2016
 */
public abstract class IController implements Initializable {

    @Override public void initialize(URL location, ResourceBundle resources) {
        checkUI();
        init(location, resources);
    }

    public abstract void init(URL location, ResourceBundle resources);

    private void checkUI(){
        for(Field field  : getClass().getDeclaredFields()) {
            try {
                if (field.isAnnotationPresent(FXML.class) && field.get(this) == null) {
                    throw new RuntimeException("Control " + field.getName() + " was not inject!");
                }
            } catch (IllegalAccessException e){
                throw new RuntimeException("Control " + field.getName() + " can not access!");
            }
        }

    }

}
