package main.java.com.rhino.app.controller;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import main.java.com.rhino.common.controller.SqlController;
import main.java.com.rhino.common.model.Campaign;
import main.java.com.rhino.common.model.SelectableSite;
import main.java.com.rhino.common.model.Site;
import main.java.com.rhino.common.utils.Log;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;

/**
 * Created by Vasiliy on 06.11.2016
 */
public class AddSiteController extends IController {

    private final static String TAG = AddSiteController.class.getSimpleName();

    @FXML Button btnDialogSiteAdd;
    @FXML Button btnDialogSiteCancel;
    @FXML TableView<SelectableSite> tvCampSites;
    @FXML TableColumn<SelectableSite, Integer> tcCampSitesId;
    @FXML TableColumn<SelectableSite, Boolean> tcCampSitesSelect;
    @FXML TableColumn<SelectableSite, String> tcCampSitesName;
    @FXML TableColumn<SelectableSite, String> tcCampSitesUrl;
    private SqlController sql = SqlController.getInstance();
    private ObservableList<SelectableSite> selectableSites;
    private Campaign camp;

    @Override
    public void init(URL location, ResourceBundle resources) {

        btnDialogSiteAdd.setOnAction(new SiteAddListener());
        btnDialogSiteCancel.setOnAction(new CancelListener());
        tcCampSitesId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcCampSitesName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tcCampSitesUrl.setCellValueFactory(new PropertyValueFactory<>("url"));
        tcCampSitesSelect.setCellValueFactory(new PropertyValueFactory<>("selected"));
        Callback<TableColumn<SelectableSite, Boolean>, TableCell<SelectableSite, Boolean>> booleanCellFactory = p -> new BooleanCell();
        tcCampSitesSelect.setCellFactory(booleanCellFactory);

        /*tcCampSitesSelect.setCellFactory(CheckBoxTableCell.forTableColumn(new Callback<Integer, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(Integer param) {
                //System.out.println("Cours "+selectableSites.get(param).getCours()+" changed value to " +items.get(param).isChecked());
                return selectableSites.get(param).selected;
            }
        }));*/

        //tvCampSites.getSelectionModel().selectedItemProperty().addListener(new CampSelectionListener());
        Log.d(TAG, "init");
    }

    void setCamp(Campaign camp) {
        this.camp = camp;
        ObservableList<Site> campSites = camp.getSites();
        ObservableList<Site> sites = sql.getSites();
        Collection<SelectableSite> selectableSitesList = new ArrayList<>();
        for (Site cs : campSites){
            for(Site s : sites)
                if(cs.getId() == s.getId()) {
                    sites.remove(s);
                    break;
                }
        }
        for(Site s : sites) selectableSitesList.add(new SelectableSite(s, null));
        selectableSites = FXCollections.observableArrayList(site -> new Observable[] {site.selectedProperty()});
        selectableSites.addAll(selectableSitesList);
        tvCampSites.setItems(selectableSites);
        Log.d(TAG, "setCamp: " + camp.getName());
    }

    class BooleanCell extends TableCell<SelectableSite, Boolean> {
        private CheckBox checkBox;
        BooleanCell() {
            checkBox = new CheckBox();
            checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (isEditing())
                    commitEdit(newValue == null ? false : newValue);
                if (newValue != null && ((TableRow<SelectableSite>) getTableRow()).getItem() != null)
                    ((TableRow<SelectableSite>) getTableRow()).getItem().selectedProperty().set(newValue);
            });
            //this.setGraphic(checkBox);
            checkBox.setAlignment(Pos.CENTER);
            setAlignment(Pos.CENTER);
            this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            this.setEditable(true);
        }

        @Override
        public void startEdit() {
            super.startEdit();
            if (isEmpty()) {
                return;
            }
            if(checkBox != null)
                checkBox.requestFocus();
        }
        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty() && checkBox != null) {
                checkBox.setSelected(item);
                boolean visible =this.getTableRow() != null && getTableRow().getIndex() < selectableSites.size();
                setGraphic(visible ? checkBox : null);
            }
        }
    }


    private class SiteAddListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            for(SelectableSite site : selectableSites){
                if(site.selectedProperty().get()){
                    Site campSite = new Site(site, camp);
                    camp.getSites().add(campSite);
                }
            }
            sql.updateCamp(camp);
            Stage stage = (Stage) btnDialogSiteAdd.getScene().getWindow();
            stage.close();
        }
    }

    private class CancelListener implements EventHandler<ActionEvent> {
        @Override public void handle(ActionEvent event) {
            Stage stage = (Stage) btnDialogSiteAdd.getScene().getWindow();
            stage.close();
        }
    }

}
