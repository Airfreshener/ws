package main.java.com.rhino.app.factory;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.paint.Paint;
import main.java.com.rhino.common.model.Site;

/**
 * Created by Vasiliy on 22.04.2016
 */
public class DefaultListCell<T> extends ListCell<T> {
    @Override public void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else if (item instanceof Node) {
            setText(null);
            Node currentNode = getGraphic();
            Node newNode = (Node) item;
            if (currentNode == null || ! currentNode.equals(newNode)) {
                setGraphic(newNode);
            }
        } else if(item instanceof Site){
            setTextFill(Paint.valueOf("#000000"));
            //boolean isStarted = ((Site) item).isStarted();
            //setText(((Site) item).getName() + (isStarted? " (running)": ""));
            setGraphic(null);
            //setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        } else {
            setText(item == null ? "null" : item.toString());
            setGraphic(null);
        }
    }
}