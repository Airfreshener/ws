package main.java.com.rhino.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.java.com.rhino.app.controller.ViewController;
import main.java.com.rhino.common.utils.Log;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.URL;
import java.security.cert.X509Certificate;

public class Main extends Application {

    private final static String TAG = Main.class.getSimpleName();

    @Override public void start(final Stage primaryStage) throws Exception{
        ClassLoader resLoader = getClass().getClassLoader();
        if(resLoader == null)
            throw new RuntimeException("Class loader can not be null");
        URL xml = resLoader.getResource("view/main.fxml");
        if(xml == null)
            throw new RuntimeException("Resource not found.");
        FXMLLoader xmlLoader = new FXMLLoader(xml);
        Parent root = xmlLoader.load();
        final ViewController viewController = xmlLoader.getController();
        //Rectangle2D screenSize = Screen.getPrimary().getVisualBounds();
        final Scene scene = new Scene(root);
        //Scene scene = new Scene(root, 1500, screenSize.getHeight() -  50);
        viewController.setScene(primaryStage);
        primaryStage.setTitle("WebScreener");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setMinHeight(600);
        primaryStage.setMinWidth(800);
        scene.getWindow().setOnCloseRequest(event -> {
            viewController.onCloseRequest();
            primaryStage.close();
            System.exit(0);
        });
    
    
        TrustManager trm = new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {return null;}
            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
        };
    
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { trm }, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    }

    //private final static int KB = 1024, LOG_SIZE = 5 * KB * KB;
    public static void main(String[] args) {
        /*
        try {
            Handler handler = new FileHandler("test.log");
            handler.setLevel(Level.ALL);
            Logger lSystem = Logger.getLogger("");
            Logger lApp = Logger.getLogger("main.java.com.rhino.app");
            lSystem.addHandler(handler);
            lApp.addHandler(handler);
            lApp.setLevel(Level.ALL);

        } catch (IOException e) {
            e.printStackTrace();
        }
        */
        try {
            launch(args);
        } catch (Throwable e) {
            Log.e("common", TAG, e);
        }
    }
}
